package co.edu.uniremington.app.entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APP_TIPO_IDENTIFICACION_TBL")
public class TipoIdentificacionEntidad {

	@Id
	@Column(name = "IN_CODIGO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;

	@Column(name = "NV_NOMBRE")
	private String nombre;

	@Column(name = "IN_EDAD_MINIMA")
	private int edadMinima;

	@Column(name = "IN_EDAD_MAXIMA")
	private int edadMaxima;

	@OneToMany(mappedBy = "numeroIdentificacion", cascade = CascadeType.ALL)
	private List<EstudianteEntidad> listaEstudianteTipoIdentificacion;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdadMinima() {
		return edadMinima;
	}

	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}

	public int getEdadMaxima() {
		return edadMaxima;
	}

	public void setEdadMaxima(int edadMaxima) {
		this.edadMaxima = edadMaxima;
	}

	public TipoIdentificacionEntidad nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public TipoIdentificacionEntidad codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public TipoIdentificacionEntidad edadMinima(int edadMinima) {
		setEdadMinima(edadMinima);
		return this;

	}

	public TipoIdentificacionEntidad edadMaxima(int edadMaxima) {
		setEdadMaxima(edadMaxima);
		return this;

	}

	public static TipoIdentificacionEntidad crear() {
		return new TipoIdentificacionEntidad();
	}

}
