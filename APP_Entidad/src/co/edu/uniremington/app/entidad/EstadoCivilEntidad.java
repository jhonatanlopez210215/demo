package co.edu.uniremington.app.entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APP_ESTADO_CIVIL_TBL")
public class EstadoCivilEntidad {
	
	@Id
	@Column(name = "IN_CODIGO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;
	
	@Column(name = "NV_NOMBRE")
	private String nombre;
	
	@OneToMany(mappedBy = "estadoCivil", cascade = CascadeType.ALL)
	private List<EstudianteEntidad> listaEstudianteEstadoCivil;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EstadoCivilEntidad nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public EstadoCivilEntidad codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public static EstadoCivilEntidad crear() {
		return new EstadoCivilEntidad();
	}

}
