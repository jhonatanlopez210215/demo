package co.edu.uniremington.app.entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APP_DEPARTAMENTO_TBL")
public class DepartamentoEntidad {

	@Id
	@Column(name = "IN_CODIGO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;

	@Column(name = "NV_NOMBRE")
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name = "IN_CODIGO_PAIS")
	private PaisEntidad pais;
	
	@OneToMany(mappedBy = "departamento", cascade = CascadeType.ALL)
	private List<CiudadEntidad> listaCiudad;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisEntidad getPais() {
		return pais;
	}

	public void setPais(PaisEntidad pais) {
		this.pais = pais;
	}

	public DepartamentoEntidad nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public DepartamentoEntidad codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public DepartamentoEntidad pais(PaisEntidad pais) {
		setPais(pais.codigo(getCodigo()));
		return this;
	}

	public static DepartamentoEntidad crear() {
		return new DepartamentoEntidad();
	}

}
