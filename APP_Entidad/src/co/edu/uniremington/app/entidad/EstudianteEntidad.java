package co.edu.uniremington.app.entidad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "APP_ESTUDIANTE_TBL")
public class EstudianteEntidad {

	@Id
	@Column(name = "IN_CODIGO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;

	@ManyToOne
	@JoinColumn(name = "IN_CODIGO_TIPO_IDENTIFICACION")
	private TipoIdentificacionEntidad tipoIdentificacion;

	@Column(name = "NV_NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;

	@Column(name = "NV_NOMBRE")
	private String nombre;

	@Column(name = "NV_CORREO_ELECTRONICO")
	private String correoElectronico;

	@Column(name = "DA_FECHA_NACIMIENTO")
	private Date fechaNacimiento;

	@ManyToOne
	@JoinColumn(name = "IN_CODIGO_ESTADO_CIVIL")
	private EstadoCivilEntidad estadoCivil;

	@Column(name = "NV_DIRECCION")
	private String direccion;

	@ManyToOne
	@JoinColumn(name = "IN_CODIGO_CIUDAD_RESIDENCIA")
	private CiudadEntidad codigoCiudad;

	// GETTS & SETTS
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public TipoIdentificacionEntidad getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(TipoIdentificacionEntidad tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public EstadoCivilEntidad getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilEntidad estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public CiudadEntidad getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setCodigoCiudad(CiudadEntidad codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	// M�todos para la capa de negocio ENTIDAD

	public EstudianteEntidad codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public EstudianteEntidad tipoIdentificacion(TipoIdentificacionEntidad tipoIdentificacion) {
		setTipoIdentificacion(tipoIdentificacion);
		return this;
	}

	public EstudianteEntidad numeroIdentificacion(String numeroIdentificacion) {
		setNumeroIdentificacion(numeroIdentificacion);
		return this;

	}

	public EstudianteEntidad nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public EstudianteEntidad correoElectronico(String correoElectronico) {
		setCorreoElectronico(correoElectronico);
		return this;

	}

	public EstudianteEntidad fechaNacimiento(Date fechaNacimiento) {
		setFechaNacimiento(fechaNacimiento);
		return this;

	}

	public EstudianteEntidad estadoCivil(EstadoCivilEntidad estadoCivil) {
		setEstadoCivil(estadoCivil);
		return this;
	}

	public EstudianteEntidad direccion(String direccion) {
		setDireccion(direccion);
		return this;

	}

	public EstudianteEntidad ciudad(CiudadEntidad ciudad) {
		setCodigoCiudad(ciudad);
		return this;
	}

	public static EstudianteEntidad crear() {
		return new EstudianteEntidad();
	}

}
