package co.edu.uniremington.app.entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "APP_CIUDAD_TBL")
public class CiudadEntidad {

	@Id
	@Column(name = "IN_CODIGO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;

	@Column(name = "NV_NOMBRE")
	private String nombre;

	@ManyToOne
	@JoinColumn(name = "IN_CODIGO_DEPARTAMENTO")
	private DepartamentoEntidad departamento;
	
	@OneToMany(mappedBy = "codigoCiudad", cascade = CascadeType.ALL)
	private List<EstudianteEntidad> listaEstudianteResidencia;


	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DepartamentoEntidad getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoEntidad departamento) {
		this.departamento = departamento;
	}

	public CiudadEntidad nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public CiudadEntidad codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public CiudadEntidad departamento(DepartamentoEntidad departamento) {
		setDepartamento(departamento);
		return this;
	}

	public static CiudadEntidad crear() {
		return new CiudadEntidad();
	}

}