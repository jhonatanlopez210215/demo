package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.IPaisJPADAO;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.negocio.fachada.IPaisFachada;

@Controller
public class PaisControlador {

	@Autowired
	private IPaisFachada paisFachada;

	@Autowired
	IPaisJPADAO repositorio;

	@RequestMapping("/pais")
	public String Consultar(Model modelo, PaisEntidad paisEntidad) {
		modelo.addAttribute("paisEntidad", new PaisEntidad());
		modelo.addAttribute("paises", repositorio.findAll());
		return "pais";

	}
	@RequestMapping("/codigoPais")
	public String consultarPais(Model modelo, PaisEntidad paisEntidad) {
		modelo.addAttribute("paisEntidad", new PaisEntidad().getCodigo());
		modelo.addAttribute("paises", repositorio.findAll());
		return "pais";

	}

	@PostMapping("/crearPais")
	public String crearPais(Model modelo, PaisEntidad paisEntidad) {
		repositorio.save(paisEntidad);
		modelo.addAttribute("paisEntidad", new PaisEntidad());
		modelo.addAttribute("paises", repositorio.findAll());
		return "pais";
	}

	@GetMapping("/editarPais/{codigo}")
	public String editarPais(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
     PaisEntidad paisParaEditar = repositorio.findById(codigo).get();
     modelo.addAttribute("paisEntidad", paisParaEditar);
     modelo.addAttribute("paises", repositorio.findAll());
     return "pais";
	}
	
	@GetMapping("/eliminarPais/{codigo}")
	public String eliminarPais(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		PaisEntidad paisParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(paisParaEliminar);
		modelo.addAttribute("paisEntidad", new PaisEntidad());
		modelo.addAttribute("paises", repositorio.findAll());
		 return "pais";
	}
}
