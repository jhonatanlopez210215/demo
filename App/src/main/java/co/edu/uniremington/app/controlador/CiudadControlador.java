package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.ICiudadJPADAO;
import co.edu.uniremington.app.entidad.CiudadEntidad;
import co.edu.uniremington.app.negocio.fachada.ICiudadFachada;

@Controller
public class CiudadControlador {

	@Autowired
	private ICiudadFachada ciudadFachada;

	@Autowired
	ICiudadJPADAO repositorio;

	@RequestMapping("/ciudad")
	public String Consultar(Model modelo, CiudadEntidad ciudadEntidad) {
		modelo.addAttribute("ciudadEntidad", new CiudadEntidad());
		modelo.addAttribute("ciudades", repositorio.findAll());
		return "ciudad";

	}

	@RequestMapping("/codigoCiudad")
	public String consultarDepartamento(Model modelo, CiudadEntidad ciudadEntidad) {
		modelo.addAttribute("ciudadEntidad", new CiudadEntidad().getCodigo());
		modelo.addAttribute("ciudades", repositorio.findAll());
		return "ciudad";
	}

	@PostMapping("/crearCiudad")
	public String crearDepartamento(Model modelo, CiudadEntidad ciudadEntidad) {
		repositorio.save(ciudadEntidad);
		modelo.addAttribute("ciudadEntidad", new CiudadEntidad());
		modelo.addAttribute("ciudades", repositorio.findAll());
		return "ciudad";
	}

	@GetMapping("/editarCiudad/{codigo}")
	public String editarCiudad(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		CiudadEntidad CiudadParaEditar = repositorio.findById(codigo).get();
		modelo.addAttribute("ciudadEntidad", CiudadParaEditar);
		modelo.addAttribute("ciudades", repositorio.findAll());
		return "ciudad";
	}

	@GetMapping("/eliminarCiudad/{codigo}")
	public String eliminarCiudad(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		CiudadEntidad ciudadParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(ciudadParaEliminar);
		modelo.addAttribute("ciudadEntidad", new CiudadEntidad());
		modelo.addAttribute("ciudades", repositorio.findAll());
		return "ciudad";
	}
}
