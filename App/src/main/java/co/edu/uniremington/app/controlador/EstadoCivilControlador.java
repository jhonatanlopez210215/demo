package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.IEstadoCivilJPADAO;
import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.negocio.fachada.IEstadoCivilFachada;

@Controller
public class EstadoCivilControlador {

	@Autowired
	private IEstadoCivilFachada estadoCivilFachada;

	@Autowired
	IEstadoCivilJPADAO repositorio;

	@RequestMapping("/estadoCivil")
	public String Consultar(Model modelo, EstadoCivilEntidad estadoCivilEntidad) {
		modelo.addAttribute("estadoCivilEntidad", new EstadoCivilEntidad());
		modelo.addAttribute("estadoCiviles", repositorio.findAll());
		return "estadoCivil";

	}

	@RequestMapping("/codigoEstadoCivil")
	public String consultarEstadoCivil(Model modelo, EstadoCivilEntidad estadoCivilEntidad) {
		modelo.addAttribute("estadoCivilEntidad", new EstadoCivilEntidad().getCodigo());
		modelo.addAttribute("estadoCiviles", repositorio.findAll());
		return "estadoCivil";

	}

	@PostMapping("/crearEstadoCivil")
	public String crearEstadoCivil(Model modelo, EstadoCivilEntidad estadoCivilEntidad) {
		repositorio.save(estadoCivilEntidad);
		modelo.addAttribute("estadoCivilEntidad", new EstadoCivilEntidad());
		modelo.addAttribute("estadoCiviles", repositorio.findAll());
		return "estadoCivil";
	}

	@GetMapping("/editarEstadoCivil/{codigo}")
	public String editarEstadoCivil(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		EstadoCivilEntidad estadoCivilParaEditar = repositorio.findById(codigo).get();
		modelo.addAttribute("estadoCivilEntidad", estadoCivilParaEditar);
		modelo.addAttribute("estadoCiviles", repositorio.findAll());
		return "estadoCivil";
	}

	@GetMapping("/eliminarEstadoCivil/{codigo}")
	public String eliminarEstadoCivil(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		EstadoCivilEntidad estadoCivilParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(estadoCivilParaEliminar);
		modelo.addAttribute("estadoCivilEntidad", new EstadoCivilEntidad());
		modelo.addAttribute("estadoCiviles", repositorio.findAll());
		return "estadoCivil";
	}
}
