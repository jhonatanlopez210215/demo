package co.edu.uniremington.app.controlador;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeControlador {
	
	@RequestMapping("/home")
	public String homeHtml(Model modelo) {
		return "home";
	}

}
