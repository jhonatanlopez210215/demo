package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.ITipoIdentificacionJPADAO;
import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;
import co.edu.uniremington.app.negocio.fachada.ITipoIdentificacionFachada;

@Controller
public class TipoIdentificacionControlador {

	@Autowired
	private ITipoIdentificacionFachada tipoIdentificacionFachada;

	@Autowired
	ITipoIdentificacionJPADAO repositorio;

	@RequestMapping("/tipoIdentificacion")
	public String Consultar(Model modelo, TipoIdentificacionEntidad tipoIdentificacionEntidad) {
		modelo.addAttribute("tipoIdentificacionEntidad", new TipoIdentificacionEntidad());
		modelo.addAttribute("tipoIdentificaciones", repositorio.findAll());
		return "tipoIdentificacion";

	}

	@RequestMapping("/codigoTipoIdentificacion")
	public String consultarTipoIdentificacion(Model modelo, TipoIdentificacionEntidad tipoIdentificacionEntidad) {
		modelo.addAttribute("tipoIdentificacionEntidad", new TipoIdentificacionEntidad().getCodigo());
		modelo.addAttribute("tipoIdentificaciones", repositorio.findAll());
		return "tipoIdentificacion";

	}

	@PostMapping("/crearTipoIdentificacion")
	public String crearTipoIdentificacion(Model modelo, TipoIdentificacionEntidad tipoIdentificacionEntidad) {
		repositorio.save(tipoIdentificacionEntidad);
		modelo.addAttribute("tipoIdentificacionEntidad", new TipoIdentificacionEntidad());
		modelo.addAttribute("tipoIdentificaciones", repositorio.findAll());
		return "tipoIdentificacion";
	}

	@GetMapping("/editarTipoIdentificacion/{codigo}")
	public String editarPais(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		TipoIdentificacionEntidad tipoIdentificacionParaEditar = repositorio.findById(codigo).get();
		modelo.addAttribute("tipoIdentificacionEntidad", tipoIdentificacionParaEditar);
		modelo.addAttribute("tipoIdentificaciones", repositorio.findAll());
		return "/tipoIdentificacion";
	}

	@GetMapping("/eliminarTipoIdentificacion/{codigo}")
	public String eliminarTipoIdentificacion(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		TipoIdentificacionEntidad tipoIdentificacionParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(tipoIdentificacionParaEliminar);
		modelo.addAttribute("tipoIdentificacionEntidad", new TipoIdentificacionEntidad());
		modelo.addAttribute("tipoIdentificaciones", repositorio.findAll());
		return "/tipoIdentificacion";
	}
}
