package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.IEstudianteJPADAO;
import co.edu.uniremington.app.entidad.EstudianteEntidad;
import co.edu.uniremington.app.negocio.fachada.IEstudianteFachada;

@Controller
public class EstudianteControlador {

	@Autowired
	private IEstudianteFachada estudianteFachada;

	@Autowired
	IEstudianteJPADAO repositorio;

	@RequestMapping("/estudiante")
	public String Consultar(Model modelo, EstudianteEntidad estudianteEntidad) {
		modelo.addAttribute("estudianteEntidad", new EstudianteEntidad());
		modelo.addAttribute("estudiantes", repositorio.findAll());
		return "estudiante";

	}

	@RequestMapping("/codigoEstudiante")
	public String consultarEstudiante(Model modelo, EstudianteEntidad estudianteEntidad) {
		modelo.addAttribute("estudianteEntidad", new EstudianteEntidad().getCodigo());
		modelo.addAttribute("estudiantes", repositorio.findAll());
		return "estudiante";

	}

	@PostMapping("/crearEstudiante")
	public String crearEstudiante(Model modelo, EstudianteEntidad estudianteEntidad) {
		repositorio.save(estudianteEntidad);
		modelo.addAttribute("estudianteEntidad", new EstudianteEntidad());
		modelo.addAttribute("estudiantes", repositorio.findAll());
		return "estudiante";
	}

	@GetMapping("/editarEstudiante/{codigo}")
	public String editarEstudiante(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		EstudianteEntidad estudianteParaEditar = repositorio.findById(codigo).get();
		modelo.addAttribute("EstudianteEntidad", estudianteParaEditar);
		modelo.addAttribute("estudiantes", repositorio.findAll());
		return "estudiante";
	}

	@GetMapping("/eliminarEstudiante/{codigo}")
	public String eliminarEstudiante(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		EstudianteEntidad estudianteParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(estudianteParaEliminar);
		modelo.addAttribute("estudianteEntidad", new EstudianteEntidad());
		modelo.addAttribute("estudiantes", repositorio.findAll());
		return "estudiante";
	}
}
