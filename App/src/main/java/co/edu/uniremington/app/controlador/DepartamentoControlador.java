package co.edu.uniremington.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.uniremington.app.datos.dao.jpa.IDepartamentoJPADAO;
import co.edu.uniremington.app.dto.DepartamentoDTO;
import co.edu.uniremington.app.entidad.DepartamentoEntidad;
import co.edu.uniremington.app.negocio.fachada.IDepartamentoFachada;

@Controller
public class DepartamentoControlador {

	@Autowired
	private IDepartamentoFachada departamentoFachada;

	@Autowired
	IDepartamentoJPADAO repositorio;

	@RequestMapping("/departamento")
	public String Consultar(Model modelo, DepartamentoEntidad departamentoEntidad) {
		modelo.addAttribute("departamentoEntidad", new DepartamentoEntidad());
		modelo.addAttribute("departamentos", repositorio.findAll());
		return "departamento";

	}
	@RequestMapping("/codigoDepartamento")
	public String consultarDepartamento(Model modelo, DepartamentoEntidad departamentoEntidad) {
		modelo.addAttribute("departamentoEntidad", new DepartamentoEntidad().getCodigo());
		modelo.addAttribute("departamentos", repositorio.findAll());
		return "departamento";

	}

	@PostMapping("/crearDepartamento")
	public String crearDepartamento(Model modelo, DepartamentoEntidad departamentoEntidad) {
		repositorio.save(departamentoEntidad);
		modelo.addAttribute("departamentoEntidad", new DepartamentoEntidad());
		modelo.addAttribute("departamentos", repositorio.findAll());
		return "departamento";
	}

	@GetMapping("/editarDepartamento/{codigo}")
	public String editarDepartamento(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
     DepartamentoEntidad departamentoParaEditar = repositorio.findById(codigo).get();
     modelo.addAttribute("departamentoEntidad", departamentoParaEditar);
     modelo.addAttribute("departamentos", repositorio.findAll());
     return "departamento";
	}
	
	@GetMapping("/eliminarDepartamento/{codigo}")
	public String eliminarDepartamento(Model modelo, @PathVariable(name = "codigo") Integer codigo) {
		DepartamentoEntidad departamentoParaEliminar = repositorio.findById(codigo).get();
		repositorio.delete(departamentoParaEliminar);
		modelo.addAttribute("departamentoEntidad", new DepartamentoEntidad());
		modelo.addAttribute("departamentos", repositorio.findAll());
		 return "departamento";
	}
}
