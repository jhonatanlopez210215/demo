package co.edu.uniremington.app.dto;

public class CiudadDTO {

	private int codigo;
	private String nombre;
	private DepartamentoDTO departamento;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DepartamentoDTO getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}

	public CiudadDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public CiudadDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public CiudadDTO departamento(DepartamentoDTO departamento) {
		setDepartamento(departamento);
		return this;
	}

	public static CiudadDTO crear() {
		return new CiudadDTO();
	}

}
