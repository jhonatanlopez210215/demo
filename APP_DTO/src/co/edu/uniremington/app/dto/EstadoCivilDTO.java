package co.edu.uniremington.app.dto;

public class EstadoCivilDTO {

	private int codigo;
	private String nombre;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EstadoCivilDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public EstadoCivilDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public static EstadoCivilDTO crear() {
		return new EstadoCivilDTO();
	}

}
