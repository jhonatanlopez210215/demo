package co.edu.uniremington.app.dto;

public class DepartamentoDTO {

	private int codigo;
	private String nombre;
	private PaisDTO pais;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}
	
	public DepartamentoDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public DepartamentoDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}
	
	public DepartamentoDTO pais(PaisDTO pais) {
		setPais(pais.codigo(getCodigo()));
		return this;
	}
	
	public static DepartamentoDTO crear() {
		return new DepartamentoDTO();
	}

}
