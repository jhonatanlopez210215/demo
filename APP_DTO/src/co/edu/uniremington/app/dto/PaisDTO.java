package co.edu.uniremington.app.dto;

public class PaisDTO {

	private int codigo;
	private String nombre;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public PaisDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}
	
	public static PaisDTO crear() {
		return new PaisDTO();
	}

}
