package co.edu.uniremington.app.dto;

import java.util.Date;

public class EstudianteDTO {

	private int codigo;
	private TipoIdentificacionDTO tipoIdentificacion;
	private String numeroIdentificacion;
	private String nombre;
	private String correoElectronico;
	private Date fechaNacimiento;
	private EstadoCivilDTO estadoCivil;
	private String direccion;
	private CiudadDTO ciudad;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public TipoIdentificacionDTO getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(TipoIdentificacionDTO tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public EstadoCivilDTO getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilDTO estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public CiudadDTO getCiudad() {
		return ciudad;
	}

	private void setCiudad(CiudadDTO ciudad) {
		this.ciudad = ciudad;
	}

	// M�todos para la capa de negocio dto

	public EstudianteDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public EstudianteDTO tipoIdentificacion(TipoIdentificacionDTO tipoIdentificacion) {
		setTipoIdentificacion(tipoIdentificacion);
		return this;
	}

	public EstudianteDTO numeroIdentificacion(String numeroIdentificacion) {
		setNumeroIdentificacion(numeroIdentificacion);
		return this;

	}

	public EstudianteDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public EstudianteDTO correoElectronico(String correoElectronico) {
		setCorreoElectronico(correoElectronico);
		return this;

	}

	public EstudianteDTO fechaNacimiento(Date fechaNacimiento) {
		setFechaNacimiento(fechaNacimiento);
		return this;

	}

	public EstudianteDTO estadoCivil(EstadoCivilDTO estadoCivil) {
		setEstadoCivil(estadoCivil);
		return this;
	}

	public EstudianteDTO direccion(String direccion) {
		setDireccion(direccion);
		return this;

	}

	public EstudianteDTO ciudad(CiudadDTO ciudad) {
		setCiudad(ciudad);
		return this;
	}

	public static EstudianteDTO crear() {
		return new EstudianteDTO();
	}

}
