package co.edu.uniremington.app.dto;

public class TipoIdentificacionDTO {

	private int codigo;
	private String nombre;
	private int edadMinima;
	private int edadMaxima;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdadMinima() {
		return edadMinima;
	}

	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}

	public int getEdadMaxima() {
		return edadMaxima;
	}

	public void setEdadMaxima(int edadMaxima) {
		this.edadMaxima = edadMaxima;
	}

	public TipoIdentificacionDTO nombre(String nombre) {
		setNombre(nombre);
		return this;

	}

	public TipoIdentificacionDTO codigo(int codigo) {
		setCodigo(codigo);
		return this;

	}

	public TipoIdentificacionDTO edadMinima(int edadMinima) {
		setEdadMinima(edadMinima);
		return this;

	}

	public TipoIdentificacionDTO edadMaxima(int edadMaxima) {
		setEdadMaxima(edadMaxima);
		return this;

	}

	public static TipoIdentificacionDTO crear() {
		return new TipoIdentificacionDTO();
	}

}
