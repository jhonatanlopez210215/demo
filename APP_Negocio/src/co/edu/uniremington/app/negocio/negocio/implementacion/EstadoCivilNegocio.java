package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uniremington.app.datos.dao.jpa.IEstadoCivilJPADAO;
import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.EstadoCivilEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.IEstadoCivilNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
public class EstadoCivilNegocio implements IEstadoCivilNegocio {

	@Autowired
	private IEstadoCivilJPADAO estadoCivilDao;

	@Override
	public void registrar(EstadoCivilDominio estadoCivilDominio) {
		EstadoCivilEntidad estadoCivilEntidad = EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(estadoCivilDominio);
		estadoCivilDao.save(estadoCivilEntidad);

	}

	@Override
	public void modificar(EstadoCivilDominio estadoCivilDominio) {
		EstadoCivilEntidad estadoCivilEntidad = EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(estadoCivilDominio);
		estadoCivilDao.save(estadoCivilEntidad);

	}

	@Override
	public void borrar(EstadoCivilDominio estadoCivilDominio) {
		EstadoCivilEntidad estadoCivilEntidad = EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(estadoCivilDominio);
		estadoCivilDao.delete(estadoCivilEntidad);

	}

	@Override
	public List<EstadoCivilDominio> consultar(EstadoCivilDominio estadoCivilDominio) {
		EstadoCivilEntidad estadoCivilEntidad = EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(estadoCivilDominio);

		return EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarListaDominios(estadoCivilDao.consultar(estadoCivilEntidad), OperacionEnumerador.POBLAMIENTO);

	}

}
