package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.EstudianteDominio;

public interface IEstudianteNegocio {

	void registrar(EstudianteDominio estudianteDominio);

	void modificar(EstudianteDominio estudianteDominio);

	void borrar(EstudianteDominio estudianteDominio);

	List<EstudianteDominio> consultar(EstudianteDominio estudianteDominio);

}
