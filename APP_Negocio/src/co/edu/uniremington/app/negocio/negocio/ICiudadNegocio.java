package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.CiudadDominio;

public interface ICiudadNegocio {

	void registrar(CiudadDominio ciudadDominio);

	void modificar(CiudadDominio ciudadDominio);

	void borrar(CiudadDominio ciudadDominio);

	List<CiudadDominio> consultar(CiudadDominio ciudadDominio);

}
