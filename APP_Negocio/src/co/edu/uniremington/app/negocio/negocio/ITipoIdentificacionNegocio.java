package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;

public interface ITipoIdentificacionNegocio {

	void registrar(TipoIdentificacionDominio tipoIdentificacionDominio);

	void modificar(TipoIdentificacionDominio tipoIdentificacionDominio);

	void borrar(TipoIdentificacionDominio tipoIdentificacionDominio);

	List<TipoIdentificacionDominio> consultar(TipoIdentificacionDominio tipoIdentificacionDominio);

}
