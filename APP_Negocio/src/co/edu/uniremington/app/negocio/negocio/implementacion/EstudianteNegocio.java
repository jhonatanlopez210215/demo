package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uniremington.app.datos.dao.jpa.IEstudianteJPADAO;
import co.edu.uniremington.app.dominio.EstudianteDominio;
import co.edu.uniremington.app.entidad.EstudianteEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.EstudianteEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.IEstudianteNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
@Service
public class EstudianteNegocio implements IEstudianteNegocio{
	
	@Autowired
	private IEstudianteJPADAO estudianteDao;
	
	@Override
	public void registrar(EstudianteDominio estudianteDominio) {
		EstudianteEntidad estudianteEntidad = EstudianteEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(estudianteDominio);
		estudianteDao.save(estudianteEntidad);
		
	}

	@Override
	public void modificar(EstudianteDominio estudianteDominio) {
		EstudianteEntidad estudianteEntidad = EstudianteEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(estudianteDominio);
		estudianteDao.save(estudianteEntidad);
		
	}

	@Override
	public void borrar(EstudianteDominio estudianteDominio) {
		EstudianteEntidad estudianteEntidad = EstudianteEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(estudianteDominio);
		estudianteDao.delete(estudianteEntidad);
		
	}
	
	

	@Override
	public List<EstudianteDominio> consultar(EstudianteDominio estudianteDominio) {
		EstudianteEntidad estudianteEntidad = EstudianteEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(estudianteDominio);
		
		return EstudianteEnsambladorEntidad.obtenerInstancia().ensamblarListaDominios(estudianteDao.consultar(estudianteEntidad),
		OperacionEnumerador.POBLAMIENTO); 
				
	}

}
