package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uniremington.app.datos.dao.jpa.IDepartamentoJPADAO;
import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.entidad.DepartamentoEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.DepartamentoEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.IDepartamentoNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
public class DepartamentoNegocio implements IDepartamentoNegocio {

	@Autowired
	private IDepartamentoJPADAO departamentoDao;

	@Override
	public void registrar(DepartamentoDominio departamentoDominio) {
		DepartamentoEntidad departamentoEntidad = DepartamentoEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(departamentoDominio);
		departamentoDao.save(departamentoEntidad);

	}

	@Override
	public void modificar(DepartamentoDominio departamentoDominio) {
		DepartamentoEntidad departamentoEntidad = DepartamentoEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(departamentoDominio);
		departamentoDao.save(departamentoEntidad);

	}

	@Override
	public void borrar(DepartamentoDominio departamentoDominio) {
		DepartamentoEntidad departamentoEntidad = DepartamentoEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(departamentoDominio);
		departamentoDao.delete(departamentoEntidad);

	}

	@Override
	public List<DepartamentoDominio> consultar(DepartamentoDominio departamentoDominio) {
		DepartamentoEntidad departamentoEntidad = DepartamentoEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(departamentoDominio);

		return DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarListaDominios(
				departamentoDao.consultar(departamentoEntidad), OperacionEnumerador.POBLAMIENTO);

	}

}
