package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.PaisDominio;

public interface IPaisNegocio {

	void registrar(PaisDominio paisDominio);

	void modificar(PaisDominio paisDominio);

	void borrar(PaisDominio paisDominio);

	List<PaisDominio> consultar(PaisDominio paisDominio);

}
