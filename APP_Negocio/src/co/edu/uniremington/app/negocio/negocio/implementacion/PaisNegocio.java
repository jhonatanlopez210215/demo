package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uniremington.app.datos.dao.jpa.IPaisJPADAO;
import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.PaisEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.IPaisNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
public class PaisNegocio implements IPaisNegocio {

	@Autowired
	private IPaisJPADAO paisDao;

	@Override
	public void registrar(PaisDominio paisDominio) {
		PaisEntidad paisEntidad = PaisEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(paisDominio);
		paisDao.save(paisEntidad);

	}

	@Override
	public void modificar(PaisDominio paisDominio) {
		PaisEntidad paisEntidad = PaisEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(paisDominio);
		paisDao.save(paisEntidad);

	}

	@Override
	public void borrar(PaisDominio paisDominio) {
		PaisEntidad paisEntidad = PaisEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(paisDominio);
		paisDao.delete(paisEntidad);

	}

	@Override
	public List<PaisDominio> consultar(PaisDominio paisDominio) {
		PaisEntidad paisEntidad = PaisEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(paisDominio);

		return PaisEnsambladorEntidad.obtenerInstancia().ensamblarListaDominios(paisDao.consultar(paisEntidad),
				OperacionEnumerador.POBLAMIENTO); 

	}

}
