package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.EstadoCivilDominio;

public interface IEstadoCivilNegocio {

	void registrar(EstadoCivilDominio estadoCivilDominio);

	void modificar(EstadoCivilDominio estadoCivilDominio);

	void borrar(EstadoCivilDominio estadoCivilDominio);

	List<EstadoCivilDominio> consultar(EstadoCivilDominio estadoCivilDominio);

}
