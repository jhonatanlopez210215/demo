package co.edu.uniremington.app.negocio.negocio;

import java.util.List;

import co.edu.uniremington.app.dominio.DepartamentoDominio;

public interface IDepartamentoNegocio {

	void registrar(DepartamentoDominio departamentoDominio);

	void modificar(DepartamentoDominio departamentoDominio);

	void borrar(DepartamentoDominio departamentoDominio);

	List<DepartamentoDominio> consultar(DepartamentoDominio departamentoDominio);

}
