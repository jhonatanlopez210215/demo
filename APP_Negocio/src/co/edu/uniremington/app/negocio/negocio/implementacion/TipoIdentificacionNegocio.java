package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import co.edu.uniremington.app.datos.dao.jpa.ITipoIdentificacionJPADAO;
import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.TipoIdentificacionEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.ITipoIdentificacionNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
public class TipoIdentificacionNegocio implements ITipoIdentificacionNegocio{
	
	@Autowired
	private ITipoIdentificacionJPADAO tipoIdentificacionDao;
	
	@Override
	public void registrar(TipoIdentificacionDominio tipoIdentificacionDominio) {
		TipoIdentificacionEntidad tipoIdentificacionEntidad = TipoIdentificacionEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(tipoIdentificacionDominio);
		tipoIdentificacionDao.save(tipoIdentificacionEntidad);
		
	}

	@Override
	public void modificar(TipoIdentificacionDominio tipoIdentificacionDominio) {
		TipoIdentificacionEntidad tipoIdentificacionEntidad = TipoIdentificacionEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(tipoIdentificacionDominio);
		tipoIdentificacionDao.save(tipoIdentificacionEntidad);
		
		
	}

	@Override
	public void borrar(TipoIdentificacionDominio tipoIdentificacionDominio) {
		TipoIdentificacionEntidad tipoIdentificacionEntidad = TipoIdentificacionEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(tipoIdentificacionDominio);
		tipoIdentificacionDao.delete(tipoIdentificacionEntidad);
		
		
	}
	
	

	@Override
	public List<TipoIdentificacionDominio> consultar(TipoIdentificacionDominio tipoIdentificacionDominio) {
		TipoIdentificacionEntidad tipoIdentificacionEntidad = TipoIdentificacionEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(tipoIdentificacionDominio);
		
		return TipoIdentificacionEnsambladorEntidad.obtenerInstancia().ensamblarListaDominios(tipoIdentificacionDao.consultar(tipoIdentificacionEntidad),
		OperacionEnumerador.POBLAMIENTO); 
				
	}

}
