package co.edu.uniremington.app.negocio.negocio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.uniremington.app.datos.dao.jpa.ICiudadJPADAO;
import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.entidad.CiudadEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion.CiudadEnsambladorEntidad;
import co.edu.uniremington.app.negocio.negocio.ICiudadNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
public class CiudadNegocio implements ICiudadNegocio {

	@Autowired
	private ICiudadJPADAO ciudadDao;

	@Override
	public void registrar(CiudadDominio ciudadDominio) {
		CiudadEntidad ciudadEntidad = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(ciudadDominio);
		ciudadDao.save(ciudadEntidad);

	}

	@Override
	public void modificar(CiudadDominio ciudadDominio) {
		CiudadEntidad ciudadEntidad = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(ciudadDominio);
		ciudadDao.save(ciudadEntidad);

	}

	@Override
	public void borrar(CiudadDominio ciudadDominio) {
		CiudadEntidad ciudadEntidad = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(ciudadDominio);
		ciudadDao.delete(ciudadEntidad);

	}

	@Override
	public List<CiudadDominio> consultar(CiudadDominio ciudadDominio) {
		CiudadEntidad ciudadEntidad = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(ciudadDominio);

		return CiudadEnsambladorEntidad.obtenerInstancia().ensamblarListaDominios(ciudadDao.consultar(ciudadEntidad),
				OperacionEnumerador.POBLAMIENTO);

	}

}
