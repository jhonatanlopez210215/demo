package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.dto.CiudadDTO;
import co.edu.uniremington.app.dto.DepartamentoDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class CiudadEnsambladorDTO implements IEnsambladorDTO<CiudadDTO, CiudadDominio> {

	public static final IEnsambladorDTO<CiudadDTO, CiudadDominio> instancia = new CiudadEnsambladorDTO();

	private CiudadEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<CiudadDTO, CiudadDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public CiudadDTO ensamblarDTO(CiudadDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de una ciudad con un dominio nulo");
		}
		DepartamentoDTO departamentoDto = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDTO(dominio.getDepartamento());

		return CiudadDTO.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre()).departamento(departamentoDto);
	}

	@Override
	public CiudadDominio ensamblarDominio(CiudadDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio de una ciudad con el objeto de transferencia de adtos nulo");
		}
		
		CiudadDominio ciudadDominio;
		DepartamentoDominio departamentoDominio;
		
		
		switch (operacion) {
		case CREACION:
			departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getDepartamento(), OperacionEnumerador.DEPENDENCIA);
			ciudadDominio = CiudadDominio.instanciaParaCreacion(dto.getNombre(), departamentoDominio);

			break;

		case ACTUALIZACION:
			departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getDepartamento(), OperacionEnumerador.DEPENDENCIA);
			ciudadDominio = CiudadDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre(), departamentoDominio);

			break;

		case ELIMINACION:
			ciudadDominio = CiudadDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getDepartamento(), OperacionEnumerador.FILTRO);
			ciudadDominio = CiudadDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre(), departamentoDominio);

			break;
		case DEPENDENCIA:
			ciudadDominio = CiudadDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getDepartamento(), OperacionEnumerador.POBLAMIENTO);
			ciudadDominio = CiudadDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre(), departamentoDominio);
		
			break;
		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return ciudadDominio;

	}

	@Override
	public List<CiudadDTO> ensamblarListaDTOs(List<CiudadDominio> listaDominios) {
		List<CiudadDTO> dtos = new ArrayList<>();

		for (CiudadDominio ciudadDominio : listaDominios) {
			dtos.add(ensamblarDTO(ciudadDominio));

		}
		return dtos;
	}
}
