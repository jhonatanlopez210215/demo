package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.dto.PaisDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class PaisEnsambladorDTO implements IEnsambladorDTO<PaisDTO, PaisDominio> {

	public static final IEnsambladorDTO<PaisDTO, PaisDominio> instancia = new PaisEnsambladorDTO();

	private PaisEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<PaisDTO, PaisDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public PaisDTO ensamblarDTO(PaisDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio("No es posible ensamblar el objeto de pais con un dominio nulo");
		}

		return PaisDTO.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre());
	}

	@Override
	public PaisDominio ensamblarDominio(PaisDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del pais con el objeto de transferencia de adtos nulo");
		}

		PaisDominio paisDominio;
		switch (operacion) {
		case CREACION:
			paisDominio = PaisDominio.instanciaParaCreacion(dto.getNombre());

			break;

		case ACTUALIZACION:
			paisDominio = PaisDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre());

			break;

		case ELIMINACION:
			paisDominio = PaisDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			paisDominio = PaisDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre());

			break;
		case DEPENDENCIA:
			paisDominio = PaisDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			paisDominio = PaisDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre());
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return paisDominio;
	}

	@Override
	public List<PaisDTO> ensamblarListaDTOs(List<PaisDominio> listaDominios) {
		List<PaisDTO> dtos = new ArrayList<>();

		for (PaisDominio paisDominio : listaDominios) {
			dtos.add(ensamblarDTO(paisDominio));

		}
		return dtos;
	}
}