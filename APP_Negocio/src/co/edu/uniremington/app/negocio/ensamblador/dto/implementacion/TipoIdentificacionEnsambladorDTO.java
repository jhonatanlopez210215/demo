package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.dto.TipoIdentificacionDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class TipoIdentificacionEnsambladorDTO
		implements IEnsambladorDTO<TipoIdentificacionDTO, TipoIdentificacionDominio> {

	public static final IEnsambladorDTO<TipoIdentificacionDTO, TipoIdentificacionDominio> instancia = new TipoIdentificacionEnsambladorDTO();

	private TipoIdentificacionEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<TipoIdentificacionDTO, TipoIdentificacionDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public TipoIdentificacionDTO ensamblarDTO(TipoIdentificacionDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de un tipo identificacion con un dominio nulo");
		}

		return TipoIdentificacionDTO.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre())
				.edadMinima(dominio.getEdadMinima()).edadMaxima(dominio.getEdadMaxima());
	}

	@Override
	public TipoIdentificacionDominio ensamblarDominio(TipoIdentificacionDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del tipo identificacion con el objeto de transferencia de adtos nulo");
		}

		TipoIdentificacionDominio tipoIdentificacionDominio;
		switch (operacion) {
		case CREACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaCreacion(dto.getNombre(), dto.getEdadMinima(), dto.getEdadMaxima());

			break;

		case ACTUALIZACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre(), dto.getEdadMinima(), dto.getEdadMaxima());

			break;

		case ELIMINACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre(), dto.getEdadMinima(), dto.getEdadMaxima());

			break;
		case DEPENDENCIA:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre(), dto.getEdadMinima(), dto.getEdadMaxima());

			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return tipoIdentificacionDominio;

	}

	@Override
	public List<TipoIdentificacionDTO> ensamblarListaDTOs(List<TipoIdentificacionDominio> listaDominios) {
		List<TipoIdentificacionDTO> dtos = new ArrayList<>();

		for (TipoIdentificacionDominio tipoIdentificaiconDominio : listaDominios) {
			dtos.add(ensamblarDTO(tipoIdentificaiconDominio));

		}
		return dtos;
	}
}
