package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class EstadoCivilEnsambladorEntidad implements IEnsambladorEntidad<EstadoCivilEntidad, EstadoCivilDominio>{

	public static final IEnsambladorEntidad<EstadoCivilEntidad, EstadoCivilDominio> instancia = new EstadoCivilEnsambladorEntidad();

	private EstadoCivilEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<EstadoCivilEntidad, EstadoCivilDominio> obtenerInstancia() {
		return instancia;
	}
	
	@Override
	public EstadoCivilEntidad ensamblarEntidad(EstadoCivilDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto entidad de datos de un estado civil con un dominio nulo");
		}

		return EstadoCivilEntidad.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre());
	}

	@Override
	public EstadoCivilDominio ensamblarDominio(EstadoCivilEntidad entidad, OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del estado civil con el objeto de transferencia de datos nulo");
		}

		EstadoCivilDominio estadoCivilDominio;
		
		switch (operacion) {
		case CREACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaCreacion(entidad.getNombre());

			break;

		case ACTUALIZACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre());

			break;

		case ELIMINACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre());

			break;
		case DEPENDENCIA:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre());
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return estadoCivilDominio;

	}

	@Override
	public List<EstadoCivilDominio> ensamblarListaDominios(List<EstadoCivilEntidad> listaEntidades,
			OperacionEnumerador operacion) {
		List<EstadoCivilDominio> dominios = new ArrayList<>();
		
		for(EstadoCivilEntidad estadoCivilEntidad : listaEntidades) {
			EstadoCivilDominio estadoCivilDominio = ensamblarDominio(estadoCivilEntidad, operacion);
			dominios.add(estadoCivilDominio);
			
		} 
		return dominios;
	}


}
