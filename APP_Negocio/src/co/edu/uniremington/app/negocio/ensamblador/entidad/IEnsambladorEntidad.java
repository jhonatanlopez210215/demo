package co.edu.uniremington.app.negocio.ensamblador.entidad;

import java.util.List;

import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

public interface IEnsambladorEntidad <E, D>{
	
	E ensamblarEntidad(D dominio);
	
	D ensamblarDominio(E entidad, OperacionEnumerador operacion);
	
	List<D> ensamblarListaDominios(List<E> listaEntidades, OperacionEnumerador operacion);

}
