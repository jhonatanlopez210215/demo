package co.edu.uniremington.app.negocio.ensamblador.dto;

import java.util.List;

import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

public interface IEnsambladorDTO<T, D> {

	T ensamblarDTO(D dominio);

	D ensamblarDominio(T dto, OperacionEnumerador operacion);
	
	List<T> ensamblarListaDTOs(List<D> listaDominios);

}
