package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.dto.DepartamentoDTO;
import co.edu.uniremington.app.dto.PaisDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class DepartamentoEnsambladorDTO implements IEnsambladorDTO<DepartamentoDTO, DepartamentoDominio> {

	public static final IEnsambladorDTO<DepartamentoDTO, DepartamentoDominio> instancia = new DepartamentoEnsambladorDTO();

	private DepartamentoEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<DepartamentoDTO, DepartamentoDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public DepartamentoDTO ensamblarDTO(DepartamentoDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de un departamento con un dominio nulo");
		}

		PaisDTO paisDto = PaisEnsambladorDTO.obtenerInstancia().ensamblarDTO(dominio.getPais());

		return DepartamentoDTO.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre()).pais(paisDto);
	}

	@Override
	public DepartamentoDominio ensamblarDominio(DepartamentoDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del departamento con el objeto de transferencia de adtos nulo");
		}

		DepartamentoDominio departamentoDominio;
		PaisDominio paisDominio;
		
		switch (operacion) {
		case CREACION:
			paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getPais(), OperacionEnumerador.DEPENDENCIA);
			departamentoDominio = DepartamentoDominio.instanciaParaCreacion(dto.getNombre(), paisDominio);

			break;

		case ACTUALIZACION:
			paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getPais(), OperacionEnumerador.DEPENDENCIA);
			departamentoDominio = DepartamentoDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre(), paisDominio);

			break;

		case ELIMINACION:
			departamentoDominio = DepartamentoDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getPais(), OperacionEnumerador.FILTRO);
			departamentoDominio = DepartamentoDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre(), paisDominio);

			break;
		case DEPENDENCIA:
			departamentoDominio = DepartamentoDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getPais(), OperacionEnumerador.POBLAMIENTO);
			departamentoDominio = DepartamentoDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre(), paisDominio);
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return departamentoDominio;

	}

	@Override
	public List<DepartamentoDTO> ensamblarListaDTOs(List<DepartamentoDominio> listaDominios) {
		List<DepartamentoDTO> dtos = new ArrayList<>();

		for (DepartamentoDominio departamentoDominio : listaDominios) {
			dtos.add(ensamblarDTO(departamentoDominio));

		}
		return dtos; 
	}
}
