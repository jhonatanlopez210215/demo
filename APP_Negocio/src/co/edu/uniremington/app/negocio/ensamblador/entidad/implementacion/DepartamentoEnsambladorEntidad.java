package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.entidad.DepartamentoEntidad;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class DepartamentoEnsambladorEntidad implements IEnsambladorEntidad<DepartamentoEntidad, DepartamentoDominio> {

	public static final IEnsambladorEntidad<DepartamentoEntidad, DepartamentoDominio> instancia = new DepartamentoEnsambladorEntidad();

	private DepartamentoEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<DepartamentoEntidad, DepartamentoDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public DepartamentoEntidad ensamblarEntidad(DepartamentoDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio("No es posible ensamblar el objeto entidad de datos de un departamento con un dominio nulo");
		}
		PaisEntidad paisEntidad = PaisEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(dominio.getPais());

		return DepartamentoEntidad.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre()).pais(paisEntidad);
	}

	@Override
	public DepartamentoDominio ensamblarDominio(DepartamentoEntidad entidad, OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio("No es posible ensamblar dominio del departamento con el objeto enttidad de datos nulo");
		}

		DepartamentoDominio departamentoDominio;
		PaisDominio paisDominio;
		
		switch (operacion) {
		case CREACION:
			paisDominio = PaisEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getPais(), OperacionEnumerador.DEPENDENCIA);
			departamentoDominio = DepartamentoDominio.instanciaParaCreacion(entidad.getNombre(), paisDominio);

			break;

		case ACTUALIZACION:
			paisDominio = PaisEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getPais(), OperacionEnumerador.DEPENDENCIA);
			departamentoDominio = DepartamentoDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre(), paisDominio);

			break;

		case ELIMINACION:
			departamentoDominio = DepartamentoDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			paisDominio = PaisEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getPais(), OperacionEnumerador.FILTRO);
			departamentoDominio = DepartamentoDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre(), paisDominio);

			break;
		case DEPENDENCIA:
			departamentoDominio = DepartamentoDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			paisDominio = PaisEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getPais(), OperacionEnumerador.POBLAMIENTO);
			departamentoDominio = DepartamentoDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre(), paisDominio);
			
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return departamentoDominio;

	}

	@Override
	public List<DepartamentoDominio> ensamblarListaDominios(List<DepartamentoEntidad> listaEntidades,
			OperacionEnumerador operacion) {
		List<DepartamentoDominio> dominios = new ArrayList<>();
		
		for(DepartamentoEntidad departamentoEntidad : listaEntidades) {
			DepartamentoDominio departamentoDominio = ensamblarDominio(departamentoEntidad, operacion);
			dominios.add(departamentoDominio);
			
		} 
		return dominios;
	}


}
