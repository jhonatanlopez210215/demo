package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.entidad.CiudadEntidad;
import co.edu.uniremington.app.entidad.DepartamentoEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class CiudadEnsambladorEntidad implements IEnsambladorEntidad<CiudadEntidad, CiudadDominio> {

	public static final IEnsambladorEntidad<CiudadEntidad, CiudadDominio> instancia = new CiudadEnsambladorEntidad();

	private CiudadEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<CiudadEntidad, CiudadDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public CiudadEntidad ensamblarEntidad(CiudadDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto entidad de datos de una ciudad con un dominio nulo");
		}

		DepartamentoEntidad departamentoEntidad = DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarEntidad(dominio.getDepartamento());

		return CiudadEntidad.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre())
				.departamento(departamentoEntidad);
	}

	@Override
	public CiudadDominio ensamblarDominio(CiudadEntidad entidad, OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio de una ciudad con el objeto entidad de datos nulo");
		}

		CiudadDominio ciudadDominio;
		DepartamentoDominio departamentoDominio;
		
		switch (operacion) {
		case CREACION:
			departamentoDominio = DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getDepartamento(), OperacionEnumerador.DEPENDENCIA);
			ciudadDominio = CiudadDominio.instanciaParaCreacion(entidad.getNombre(), departamentoDominio);

			break;

		case ACTUALIZACION:
			departamentoDominio = DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getDepartamento(), OperacionEnumerador.DEPENDENCIA);
			ciudadDominio = CiudadDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre(), departamentoDominio);

			break;

		case ELIMINACION:
			ciudadDominio = CiudadDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			departamentoDominio = DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getDepartamento(), OperacionEnumerador.FILTRO);
			ciudadDominio = CiudadDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre(), departamentoDominio);

			break;
		case DEPENDENCIA:
			ciudadDominio = CiudadDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			departamentoDominio = DepartamentoEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getDepartamento(), OperacionEnumerador.POBLAMIENTO);
			ciudadDominio = CiudadDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre(), departamentoDominio);
			
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return ciudadDominio;

	}

	@Override
	public List<CiudadDominio> ensamblarListaDominios(List<CiudadEntidad> listaEntidades,
			OperacionEnumerador operacion) {
		List<CiudadDominio> dominios = new ArrayList<>();
		
		for(CiudadEntidad ciudadEntidad : listaEntidades) {
			CiudadDominio ciudadDominio = ensamblarDominio(ciudadEntidad, operacion);
			dominios.add(ciudadDominio);
			
		} 
		return dominios;
	}


}
