package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class TipoIdentificacionEnsambladorEntidad implements IEnsambladorEntidad<TipoIdentificacionEntidad, TipoIdentificacionDominio> {

	public static final IEnsambladorEntidad<TipoIdentificacionEntidad, TipoIdentificacionDominio> instancia = new TipoIdentificacionEnsambladorEntidad();

	private TipoIdentificacionEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<TipoIdentificacionEntidad, TipoIdentificacionDominio> obtenerInstancia() {
		return instancia;
	}
	
	@Override
	public TipoIdentificacionEntidad ensamblarEntidad(TipoIdentificacionDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de un tipo identificacion con un dominio nulo");
		}

		return TipoIdentificacionEntidad.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre())
				.edadMinima(dominio.getEdadMinima()).edadMaxima(dominio.getEdadMaxima());
	}

	@Override
	public TipoIdentificacionDominio ensamblarDominio(TipoIdentificacionEntidad entidad,
			OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del tipo identificacion con el objeto de transferencia de adtos nulo");
		}

		TipoIdentificacionDominio tipoIdentificacionDominio;
		
		switch (operacion) {
		case CREACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaCreacion(entidad.getNombre(), entidad.getEdadMinima(), entidad.getEdadMaxima());

			break;

		case ACTUALIZACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre(), entidad.getEdadMinima(), entidad.getEdadMaxima());

			break;

		case ELIMINACION:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre(), entidad.getEdadMinima(), entidad.getEdadMaxima());

			break;
		case DEPENDENCIA:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			tipoIdentificacionDominio = TipoIdentificacionDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre(), entidad.getEdadMinima(), entidad.getEdadMaxima());
			
			break;
		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return tipoIdentificacionDominio;

	}

	@Override
	public List<TipoIdentificacionDominio> ensamblarListaDominios(List<TipoIdentificacionEntidad> listaEntidades,
			OperacionEnumerador operacion) {
		List<TipoIdentificacionDominio> dominios = new ArrayList<>();
		
		for(TipoIdentificacionEntidad tipoIdentificacionEntidad : listaEntidades) {
			TipoIdentificacionDominio tipoIdentificacionDominio = ensamblarDominio(tipoIdentificacionEntidad, operacion);
			dominios.add(tipoIdentificacionDominio);
			
		} 
		return dominios;
	}


}
