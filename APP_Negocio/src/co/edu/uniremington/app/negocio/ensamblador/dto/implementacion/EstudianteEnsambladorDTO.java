package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.dominio.EstudianteDominio;
import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.dto.CiudadDTO;
import co.edu.uniremington.app.dto.EstadoCivilDTO;
import co.edu.uniremington.app.dto.EstudianteDTO;
import co.edu.uniremington.app.dto.TipoIdentificacionDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class EstudianteEnsambladorDTO implements IEnsambladorDTO<EstudianteDTO, EstudianteDominio> {

	public static final IEnsambladorDTO<EstudianteDTO, EstudianteDominio> instancia = new EstudianteEnsambladorDTO();

	private EstudianteEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<EstudianteDTO, EstudianteDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public EstudianteDTO ensamblarDTO(EstudianteDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de un estudiante con un dominio nulo");
		}

		TipoIdentificacionDTO tipoIdentificacionDto = TipoIdentificacionEnsambladorDTO.obtenerInstancia()
				.ensamblarDTO(dominio.getTipoIdentificacion());

		EstadoCivilDTO estadoCivilDto = EstadoCivilEnsambladorDTO.obtenerInstancia()
				.ensamblarDTO(dominio.getEstadoCivil());

		CiudadDTO ciudadDto = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDTO(dominio.getCodigoCiudad());

		return EstudianteDTO.crear().codigo(dominio.getCodigo()).tipoIdentificacion(tipoIdentificacionDto)
				.numeroIdentificacion(dominio.getNumeroIdentificacion()).nombre(dominio.getNombre())
				.correoElectronico(dominio.getCorreoElectronico()).fechaNacimiento(dominio.getFechaNacimiento())
				.estadoCivil(estadoCivilDto).direccion(dominio.getDireccion()).ciudad(ciudadDto);
	}

	@Override
	public EstudianteDominio ensamblarDominio(EstudianteDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio de un estudiante con el objeto de transferencia de datos nulo");
		}

		EstudianteDominio estudianteDominio;
		TipoIdentificacionDominio tipoIdentificacionDominio;
		EstadoCivilDominio estadoCivilDominio;
		CiudadDominio ciudadDominio;

		switch (operacion) {
		case CREACION:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia()
					.ensamblarDominio(dto.getTipoIdentificacion(), OperacionEnumerador.DEPENDENCIA);

			estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getEstadoCivil(),
					OperacionEnumerador.DEPENDENCIA);

			ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getCiudad(),
					OperacionEnumerador.DEPENDENCIA);

			estudianteDominio = EstudianteDominio.instanciaParaCreacion(dto.getNombre(), tipoIdentificacionDominio,
					dto.getNumeroIdentificacion(), dto.getCorreoElectronico(), dto.getFechaNacimiento(),
					estadoCivilDominio, dto.getDireccion(), ciudadDominio);

			break;

		case ACTUALIZACION:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia()
					.ensamblarDominio(dto.getTipoIdentificacion(), OperacionEnumerador.DEPENDENCIA);

			estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getEstadoCivil(),
					OperacionEnumerador.DEPENDENCIA);

			ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getCiudad(),
					OperacionEnumerador.DEPENDENCIA);

			estudianteDominio = EstudianteDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre(),
					tipoIdentificacionDominio, dto.getNumeroIdentificacion(), dto.getCorreoElectronico(),
					dto.getFechaNacimiento(), estadoCivilDominio, dto.getDireccion(), ciudadDominio);
			break;

		case ELIMINACION:
			estudianteDominio = EstudianteDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia()
					.ensamblarDominio(dto.getTipoIdentificacion(), OperacionEnumerador.FILTRO);

			estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getEstadoCivil(),
					OperacionEnumerador.FILTRO);

			ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getCiudad(),
					OperacionEnumerador.FILTRO);

			estudianteDominio = EstudianteDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre(),
					tipoIdentificacionDominio, dto.getNumeroIdentificacion(), dto.getCorreoElectronico(),
					dto.getFechaNacimiento(), estadoCivilDominio, dto.getDireccion(), ciudadDominio);

			break;
		case DEPENDENCIA:
			estudianteDominio = EstudianteDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia()
					.ensamblarDominio(dto.getTipoIdentificacion(), OperacionEnumerador.POBLAMIENTO);

			estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getEstadoCivil(),
					OperacionEnumerador.POBLAMIENTO);

			ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(dto.getCiudad(),
					OperacionEnumerador.POBLAMIENTO);

			estudianteDominio = EstudianteDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre(),
					tipoIdentificacionDominio, dto.getNumeroIdentificacion(), dto.getCorreoElectronico(),
					dto.getFechaNacimiento(), estadoCivilDominio, dto.getDireccion(), ciudadDominio);
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return estudianteDominio;

	}

	@Override
	public List<EstudianteDTO> ensamblarListaDTOs(List<EstudianteDominio> listaDominios) {
		List<EstudianteDTO> dtos = new ArrayList<>();

		for (EstudianteDominio estudianteDominio : listaDominios) {
			dtos.add(ensamblarDTO(estudianteDominio));

		}
		return dtos;
	}
}
