package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class PaisEnsambladorEntidad implements IEnsambladorEntidad<PaisEntidad, PaisDominio> {
	
	public static final IEnsambladorEntidad<PaisEntidad, PaisDominio> instancia = new PaisEnsambladorEntidad();

	private PaisEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<PaisEntidad, PaisDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public PaisEntidad ensamblarEntidad(PaisDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio("No es posible ensamblar el objeto de entidad pais con un dominio nulo");
		}
		return PaisEntidad.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre());
	}

	@Override
	public PaisDominio ensamblarDominio(PaisEntidad entidad, OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del pais con el objeto entidad de datos nulo");
		}

		PaisDominio paisDominio;
		switch (operacion) {
		case CREACION:
			paisDominio = PaisDominio.instanciaParaCreacion(entidad.getNombre());

			break;

		case ACTUALIZACION:
			paisDominio = PaisDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre());

			break;

		case ELIMINACION:
			paisDominio = PaisDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			paisDominio = PaisDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre());

			break;
		case DEPENDENCIA:
			paisDominio = PaisDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			paisDominio = PaisDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre());
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return paisDominio;
	}

	@Override
	public List<PaisDominio> ensamblarListaDominios(List<PaisEntidad> listaEntidades, OperacionEnumerador operacion) {
		List<PaisDominio> dominios = new ArrayList<>();
		
		for(PaisEntidad paisEntidad : listaEntidades) {
			PaisDominio paisDominio = ensamblarDominio(paisEntidad, operacion);
			dominios.add(paisDominio);
			
		} 
		return dominios;
	}



	
	



}
