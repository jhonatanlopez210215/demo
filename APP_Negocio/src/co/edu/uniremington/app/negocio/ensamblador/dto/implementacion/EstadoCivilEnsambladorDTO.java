package co.edu.uniremington.app.negocio.ensamblador.dto.implementacion;


import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.dto.EstadoCivilDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.IEnsambladorDTO;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class EstadoCivilEnsambladorDTO implements IEnsambladorDTO<EstadoCivilDTO, EstadoCivilDominio> {

	public static final IEnsambladorDTO<EstadoCivilDTO, EstadoCivilDominio> instancia = new EstadoCivilEnsambladorDTO();

	private EstadoCivilEnsambladorDTO() {
		super();

	}

	public static IEnsambladorDTO<EstadoCivilDTO, EstadoCivilDominio> obtenerInstancia() {
		return instancia;
	}
	
	@Override
	public EstadoCivilDTO ensamblarDTO(EstadoCivilDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto de transporte de datos de un estado civil con un dominio nulo");
		}

		return EstadoCivilDTO.crear().codigo(dominio.getCodigo()).nombre(dominio.getNombre());
	}

	@Override
	public EstadoCivilDominio ensamblarDominio(EstadoCivilDTO dto, OperacionEnumerador operacion) {
		if (dto == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio del estado civil con el objeto de transferencia de adtos nulo");
		}

		EstadoCivilDominio estadoCivilDominio;
		switch (operacion) {
		case CREACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaCreacion(dto.getNombre());

			break;

		case ACTUALIZACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaActualizacion(dto.getCodigo(), dto.getNombre());

			break;

		case ELIMINACION:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaEliminacion(dto.getCodigo());

			break;

		case FILTRO:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaFiltro(dto.getCodigo(), dto.getNombre());

			break;
		case DEPENDENCIA:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaDependencia(dto.getCodigo());

			break;
		case POBLAMIENTO:
			estadoCivilDominio = EstadoCivilDominio.instanciaParaPoblamiento(dto.getCodigo(), dto.getNombre());
			break;

		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return estadoCivilDominio;

	}

	@Override
	public List<EstadoCivilDTO> ensamblarListaDTOs(List<EstadoCivilDominio> listaDominios) {
		List<EstadoCivilDTO> dtos = new ArrayList<>();

		for (EstadoCivilDominio estadoCivilDominio : listaDominios) {
			dtos.add(ensamblarDTO(estadoCivilDominio));

		}
		return dtos;
	}
}
