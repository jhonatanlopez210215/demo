package co.edu.uniremington.app.negocio.ensamblador.entidad.implementacion;

import java.util.ArrayList;
import java.util.List;

import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.dominio.EstudianteDominio;
import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.entidad.CiudadEntidad;
import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
import co.edu.uniremington.app.entidad.EstudianteEntidad;
import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;
import co.edu.uniremington.app.negocio.ensamblador.entidad.IEnsambladorEntidad;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesNegocio;

public class EstudianteEnsambladorEntidad implements IEnsambladorEntidad<EstudianteEntidad, EstudianteDominio> {

	public static final IEnsambladorEntidad<EstudianteEntidad, EstudianteDominio> instancia = new EstudianteEnsambladorEntidad();

	private EstudianteEnsambladorEntidad() {
		super();
	}

	public static IEnsambladorEntidad<EstudianteEntidad, EstudianteDominio> obtenerInstancia() {
		return instancia;
	}

	@Override
	public EstudianteEntidad ensamblarEntidad(EstudianteDominio dominio) {
		if (dominio == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar el objeto entidad de datos de un estudiante con un dominio nulo");
		}

		TipoIdentificacionEntidad tipoIdentificacionEntidad = TipoIdentificacionEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(dominio.getTipoIdentificacion());

		EstadoCivilEntidad estadoCivilEntidad = EstadoCivilEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(dominio.getEstadoCivil());

		CiudadEntidad ciudadEntidad = CiudadEnsambladorEntidad.obtenerInstancia()
				.ensamblarEntidad(dominio.getCodigoCiudad());

		return EstudianteEntidad.crear().codigo(dominio.getCodigo()).tipoIdentificacion(tipoIdentificacionEntidad)
				.numeroIdentificacion(dominio.getNumeroIdentificacion()).nombre(dominio.getNombre())
				.correoElectronico(dominio.getCorreoElectronico()).fechaNacimiento(dominio.getFechaNacimiento())
				.estadoCivil(estadoCivilEntidad).direccion(dominio.getDireccion()).ciudad(ciudadEntidad);
	}

	@Override
	public EstudianteDominio ensamblarDominio(EstudianteEntidad entidad, OperacionEnumerador operacion) {
		if (entidad == null) {
			throw new ExcepcionesNegocio(
					"No es posible ensamblar dominio de un estudiante con el objeto entidad de datos nulo");
		}

		EstudianteDominio estudianteDominio;
		TipoIdentificacionDominio tipoIdentificacionDominio;
		EstadoCivilDominio estadoCivilDominio;
		CiudadDominio ciudadDominio;

		switch (operacion) {
		case CREACION:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getTipoIdentificacion(), OperacionEnumerador.DEPENDENCIA);

			estadoCivilDominio = EstadoCivilEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getEstadoCivil(), OperacionEnumerador.DEPENDENCIA);

			ciudadDominio = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getCodigoCiudad(),
					OperacionEnumerador.DEPENDENCIA);

			estudianteDominio = EstudianteDominio.instanciaParaCreacion(entidad.getNombre(), tipoIdentificacionDominio,
					entidad.getNumeroIdentificacion(), entidad.getCorreoElectronico(), entidad.getFechaNacimiento(),
					estadoCivilDominio, entidad.getDireccion(), ciudadDominio);

			break;

		case ACTUALIZACION:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getTipoIdentificacion(), OperacionEnumerador.DEPENDENCIA);

			estadoCivilDominio = EstadoCivilEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getEstadoCivil(), OperacionEnumerador.DEPENDENCIA);

			ciudadDominio = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getCodigoCiudad(),
					OperacionEnumerador.DEPENDENCIA);

			estudianteDominio = EstudianteDominio.instanciaParaActualizacion(entidad.getCodigo(), entidad.getNombre(),
					tipoIdentificacionDominio, entidad.getNumeroIdentificacion(), entidad.getCorreoElectronico(),
					entidad.getFechaNacimiento(), estadoCivilDominio, entidad.getDireccion(), ciudadDominio);

			break;

		case ELIMINACION:
			estudianteDominio = EstudianteDominio.instanciaParaEliminacion(entidad.getCodigo());

			break;

		case FILTRO:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getTipoIdentificacion(), OperacionEnumerador.FILTRO);

			estadoCivilDominio = EstadoCivilEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getEstadoCivil(), OperacionEnumerador.FILTRO);

			ciudadDominio = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getCodigoCiudad(),
					OperacionEnumerador.FILTRO);

			estudianteDominio = EstudianteDominio.instanciaParaFiltro(entidad.getCodigo(), entidad.getNombre(),
					tipoIdentificacionDominio, entidad.getNumeroIdentificacion(), entidad.getCorreoElectronico(),
					entidad.getFechaNacimiento(), estadoCivilDominio, entidad.getDireccion(), ciudadDominio);

			break;
		case DEPENDENCIA:
			estudianteDominio = EstudianteDominio.instanciaParaDependencia(entidad.getCodigo());

			break;
		case POBLAMIENTO:
			tipoIdentificacionDominio = TipoIdentificacionEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getTipoIdentificacion(), OperacionEnumerador.POBLAMIENTO);

			estadoCivilDominio = EstadoCivilEnsambladorEntidad.obtenerInstancia()
					.ensamblarDominio(entidad.getEstadoCivil(), OperacionEnumerador.POBLAMIENTO);

			ciudadDominio = CiudadEnsambladorEntidad.obtenerInstancia().ensamblarDominio(entidad.getCodigoCiudad(),
					OperacionEnumerador.POBLAMIENTO);

			estudianteDominio = EstudianteDominio.instanciaParaPoblamiento(entidad.getCodigo(), entidad.getNombre(),
					tipoIdentificacionDominio, entidad.getNumeroIdentificacion(), entidad.getCorreoElectronico(),
					entidad.getFechaNacimiento(), estadoCivilDominio, entidad.getDireccion(), ciudadDominio);

			break;
		default:
			throw new ExcepcionesNegocio("Operacion para ensamblar dominio no configurada");
		}
		return estudianteDominio;

	}

	@Override
	public List<EstudianteDominio> ensamblarListaDominios(List<EstudianteEntidad> listaEntidades,
			OperacionEnumerador operacion) {
		List<EstudianteDominio> dominios = new ArrayList<>();

		for (EstudianteEntidad estudianteEntidad : listaEntidades) {
			EstudianteDominio estudianteDominio = ensamblarDominio(estudianteEntidad, operacion);
			dominios.add(estudianteDominio);

		}
		return dominios;
	}

}
