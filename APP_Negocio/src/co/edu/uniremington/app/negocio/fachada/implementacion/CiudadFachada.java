package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.CiudadDominio;
import co.edu.uniremington.app.dto.CiudadDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.CiudadEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.ICiudadFachada;
import co.edu.uniremington.app.negocio.negocio.ICiudadNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional // Se encarga de abrir, confirmar, cancelar y cerrar conexi�n
public class CiudadFachada implements ICiudadFachada {

	@Autowired
	private ICiudadNegocio ciudadNegocio;

	@Override
	public void registrar(CiudadDTO ciudadDto) {
		CiudadDominio ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(ciudadDto,OperacionEnumerador.CREACION);
		ciudadNegocio.registrar(ciudadDominio);

	}

	@Override
	public void actualizar(CiudadDTO ciudadDto) {
		CiudadDominio ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(ciudadDto,OperacionEnumerador.ACTUALIZACION);
		ciudadNegocio.modificar(ciudadDominio);

	}

	@Override
	public void eliminar(CiudadDTO ciudadDto) {
		CiudadDominio ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(ciudadDto,OperacionEnumerador.ELIMINACION);
		ciudadNegocio.borrar(ciudadDominio);

	}

	@Override
	public List<CiudadDTO> consultar(CiudadDTO ciudadDto) {
		CiudadDominio ciudadDominio = CiudadEnsambladorDTO.obtenerInstancia().ensamblarDominio(ciudadDto,OperacionEnumerador.FILTRO);
		List<CiudadDominio> listaCiudadesDominio = ciudadNegocio.consultar(ciudadDominio);
		
		return CiudadEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaCiudadesDominio);
	} 

}
