package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.TipoIdentificacionDTO;

public interface ITipoIdentificacionFachada {

	void registrar(TipoIdentificacionDTO tipoIdentificacionDto);

	void actualizar(TipoIdentificacionDTO tipoIdentificacionDto);

	void eliminar(TipoIdentificacionDTO tipoIdentificacionDto);

	List<TipoIdentificacionDTO> consultar(TipoIdentificacionDTO tipoIdentificacionDto);

}
