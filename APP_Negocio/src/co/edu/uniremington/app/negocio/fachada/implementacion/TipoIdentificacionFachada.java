package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.TipoIdentificacionDominio;
import co.edu.uniremington.app.dto.TipoIdentificacionDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.TipoIdentificacionEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.ITipoIdentificacionFachada;
import co.edu.uniremington.app.negocio.negocio.ITipoIdentificacionNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional
public class TipoIdentificacionFachada implements ITipoIdentificacionFachada {

	@Autowired
	private ITipoIdentificacionNegocio tipoIdentificacionNegocio;

	@Override
	public void registrar(TipoIdentificacionDTO tipoIdentificacionDto) {
		TipoIdentificacionDominio tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia().ensamblarDominio(tipoIdentificacionDto,OperacionEnumerador.CREACION);
		tipoIdentificacionNegocio.registrar(tipoIdentificacionDominio);

	}

	@Override
	public void actualizar(TipoIdentificacionDTO tipoIdentificacionDto) {
		TipoIdentificacionDominio tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia().ensamblarDominio(tipoIdentificacionDto,OperacionEnumerador.ACTUALIZACION);
		tipoIdentificacionNegocio.modificar(tipoIdentificacionDominio);

	}

	@Override
	public void eliminar(TipoIdentificacionDTO tipoIdentificacionDto) {
		TipoIdentificacionDominio tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia().ensamblarDominio(tipoIdentificacionDto,OperacionEnumerador.ELIMINACION);
		tipoIdentificacionNegocio.borrar(tipoIdentificacionDominio);

	}

	@Override
	public List<TipoIdentificacionDTO> consultar(TipoIdentificacionDTO tipoIdentificacionDto) {
		TipoIdentificacionDominio tipoIdentificacionDominio = TipoIdentificacionEnsambladorDTO.obtenerInstancia().ensamblarDominio(tipoIdentificacionDto,OperacionEnumerador.FILTRO);
		List<TipoIdentificacionDominio> listaTiposDeIdentificacionDominio = tipoIdentificacionNegocio.consultar(tipoIdentificacionDominio);
		
		return TipoIdentificacionEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaTiposDeIdentificacionDominio);
	} 

}
