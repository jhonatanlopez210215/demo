package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.DepartamentoDominio;
import co.edu.uniremington.app.dto.DepartamentoDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.DepartamentoEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.IDepartamentoFachada;
import co.edu.uniremington.app.negocio.negocio.IDepartamentoNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional // Se encarga de abrir, confirmar, cancelar y cerrar conexi�n
public class DepartamentoFachada implements IDepartamentoFachada {

	@Autowired
	private IDepartamentoNegocio departamentoNegocio;

	@Override
	public void registrar(DepartamentoDTO departamentoDto) {
		DepartamentoDominio departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(departamentoDto,OperacionEnumerador.CREACION);
		departamentoNegocio.registrar(departamentoDominio);

	}

	@Override
	public void actualizar(DepartamentoDTO departamentoDto) {
		DepartamentoDominio departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(departamentoDto,OperacionEnumerador.ACTUALIZACION);
		departamentoNegocio.modificar(departamentoDominio);

	}

	@Override
	public void eliminar(DepartamentoDTO departamentoDto) {
		DepartamentoDominio departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(departamentoDto,OperacionEnumerador.ELIMINACION);
		departamentoNegocio.borrar(departamentoDominio);

	}

	@Override
	public List<DepartamentoDTO> consultar(DepartamentoDTO departamentoDto) {
		DepartamentoDominio departamentoDominio = DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarDominio(departamentoDto,OperacionEnumerador.FILTRO);
		List<DepartamentoDominio> listaDepartamentosDominio = departamentoNegocio.consultar(departamentoDominio);
		
		return DepartamentoEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaDepartamentosDominio);
	} 

}
