package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.EstadoCivilDTO;

public interface IEstadoCivilFachada {

	void registrar(EstadoCivilDTO estadoCivilDto);

	void actualizar(EstadoCivilDTO estadoCivilDto);

	void eliminar(EstadoCivilDTO estadoCivilDto);

	List<EstadoCivilDTO> consultar(EstadoCivilDTO estadoCivilDto);

}
