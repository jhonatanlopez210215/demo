package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.DepartamentoDTO;

public interface IDepartamentoFachada {

	void registrar(DepartamentoDTO departamentoDto);

	void actualizar(DepartamentoDTO departamentoDto);

	void eliminar(DepartamentoDTO departamentoDto);

	List<DepartamentoDTO> consultar(DepartamentoDTO departamentoDto);

}
