package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.EstudianteDTO;

public interface IEstudianteFachada {

	void registrar(EstudianteDTO estudianteDto);

	void actualizar(EstudianteDTO estudianteDto);

	void eliminar(EstudianteDTO estudianteDto);

	List<EstudianteDTO> consultar(EstudianteDTO estudianteDto);

}
