package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.EstadoCivilDominio;
import co.edu.uniremington.app.dto.EstadoCivilDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.EstadoCivilEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.IEstadoCivilFachada;
import co.edu.uniremington.app.negocio.negocio.IEstadoCivilNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional 
public class EstadoCivilFachada implements IEstadoCivilFachada {

	@Autowired
	private IEstadoCivilNegocio estadoCivilNegocio;

	@Override
	public void registrar(EstadoCivilDTO estadoCivilDto) {
		EstadoCivilDominio estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(estadoCivilDto,OperacionEnumerador.CREACION);
		estadoCivilNegocio.registrar(estadoCivilDominio);

	}

	@Override
	public void actualizar(EstadoCivilDTO estadoCivilDto) {
		EstadoCivilDominio estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(estadoCivilDto,OperacionEnumerador.ACTUALIZACION);
		estadoCivilNegocio.modificar(estadoCivilDominio);

	}

	@Override
	public void eliminar(EstadoCivilDTO estadoCivilDto) {
		EstadoCivilDominio estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(estadoCivilDto,OperacionEnumerador.ELIMINACION);
		estadoCivilNegocio.borrar(estadoCivilDominio);

	}

	@Override
	public List<EstadoCivilDTO> consultar(EstadoCivilDTO estadoCivilDto) {
		EstadoCivilDominio estadoCivilDominio = EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarDominio(estadoCivilDto,OperacionEnumerador.FILTRO);
		List<EstadoCivilDominio> listaEstadosCivilesDominio = estadoCivilNegocio.consultar(estadoCivilDominio);
		
		return EstadoCivilEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaEstadosCivilesDominio);
	} 

}
