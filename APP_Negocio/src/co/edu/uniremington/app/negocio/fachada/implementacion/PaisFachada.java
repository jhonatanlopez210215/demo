package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.PaisDominio;
import co.edu.uniremington.app.dto.PaisDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.PaisEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.IPaisFachada;
import co.edu.uniremington.app.negocio.negocio.IPaisNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional // Se encarga de abrir, confirmar, cancelar y cerrar conexi�n
public class PaisFachada implements IPaisFachada {

	@Autowired
	private IPaisNegocio paisNegocio;

	@Override
	public void registrar(PaisDTO paisDto) {
		PaisDominio paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(paisDto,OperacionEnumerador.CREACION);
		paisNegocio.registrar(paisDominio);

	}

	@Override
	public void actualizar(PaisDTO paisDto) {
		PaisDominio paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(paisDto,OperacionEnumerador.ACTUALIZACION);
		paisNegocio.modificar(paisDominio);

	}

	@Override
	public void eliminar(PaisDTO paisDto) {
		PaisDominio paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(paisDto,OperacionEnumerador.ELIMINACION);
		paisNegocio.borrar(paisDominio);

	}

	@Override
	public List<PaisDTO> consultar(PaisDTO paisDto) {
		PaisDominio paisDominio = PaisEnsambladorDTO.obtenerInstancia().ensamblarDominio(paisDto,OperacionEnumerador.FILTRO);
		List<PaisDominio> listaPaisesDominio = paisNegocio.consultar(paisDominio);
		
		return PaisEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaPaisesDominio);
	} 

}
