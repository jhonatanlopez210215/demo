package co.edu.uniremington.app.negocio.fachada.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.uniremington.app.dominio.EstudianteDominio;
import co.edu.uniremington.app.dto.EstudianteDTO;
import co.edu.uniremington.app.negocio.ensamblador.dto.implementacion.EstudianteEnsambladorDTO;
import co.edu.uniremington.app.negocio.fachada.IEstudianteFachada;
import co.edu.uniremington.app.negocio.negocio.IEstudianteNegocio;
import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;

@Service
@Transactional // Se encarga de abrir, confirmar, cancelar y cerrar conexi�n
public class EstudianteFachada implements IEstudianteFachada {

	@Autowired
	private IEstudianteNegocio estudianteNegocio;

	@Override
	public void registrar(EstudianteDTO estudianteDto) {
		EstudianteDominio estudianteDominio = EstudianteEnsambladorDTO.obtenerInstancia().ensamblarDominio(estudianteDto,OperacionEnumerador.CREACION);
		estudianteNegocio.registrar(estudianteDominio);

	}

	@Override
	public void actualizar(EstudianteDTO estudianteDto) {
		EstudianteDominio estudianteDominio = EstudianteEnsambladorDTO.obtenerInstancia().ensamblarDominio(estudianteDto,OperacionEnumerador.ACTUALIZACION);
		estudianteNegocio.modificar(estudianteDominio);

	}

	@Override
	public void eliminar(EstudianteDTO estudianteDto) {
		EstudianteDominio estudianteDominio = EstudianteEnsambladorDTO.obtenerInstancia().ensamblarDominio(estudianteDto,OperacionEnumerador.ELIMINACION);
		estudianteNegocio.borrar(estudianteDominio);

	}

	@Override
	public List<EstudianteDTO> consultar(EstudianteDTO estudianteDto) {
		EstudianteDominio estudianteDominio = EstudianteEnsambladorDTO.obtenerInstancia().ensamblarDominio(estudianteDto,OperacionEnumerador.FILTRO);
		List<EstudianteDominio> listaEstudiantesDominio = estudianteNegocio.consultar(estudianteDominio);
		
		return EstudianteEnsambladorDTO.obtenerInstancia().ensamblarListaDTOs(listaEstudiantesDominio);
	} 

}
