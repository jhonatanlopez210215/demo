package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.PaisDTO;

public interface IPaisFachada {

	void registrar(PaisDTO paisDto);

	void actualizar(PaisDTO paisDto);

	void eliminar(PaisDTO paisDto);

	List<PaisDTO> consultar(PaisDTO paisDto);

}
