package co.edu.uniremington.app.negocio.fachada;

import java.util.List;

import co.edu.uniremington.app.dto.CiudadDTO;

public interface ICiudadFachada {

	void registrar(CiudadDTO ciudadDto);

	void actualizar(CiudadDTO ciudadDto);

	void eliminar(CiudadDTO ciudadDto);

	List<CiudadDTO> consultar(CiudadDTO ciudadDto);

}
