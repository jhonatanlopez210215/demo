package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.PaisEntidad;

@Repository
public interface IPaisJPADAOCustom {
	List<PaisEntidad> consultar(PaisEntidad paisEntidad);
	

}
