package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


import co.edu.uniremington.app.datos.dao.jpa.ITipoIdentificacionJPADAOCustom;
import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class ITipoIdentificacionJPADAOImpl implements ITipoIdentificacionJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<TipoIdentificacionEntidad> consultar(TipoIdentificacionEntidad tipoIdentificacionEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<TipoIdentificacionEntidad> consulta = constructorCondiciones.createQuery(TipoIdentificacionEntidad.class);
			Root<TipoIdentificacionEntidad> condicional = consulta.from(TipoIdentificacionEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (tipoIdentificacionEntidad != null) {

				if (tipoIdentificacionEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), tipoIdentificacionEntidad.getCodigo()));

				}

				if (tipoIdentificacionEntidad.getNombre() != null && tipoIdentificacionEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), tipoIdentificacionEntidad.getNombre()));
				}
				
				if (tipoIdentificacionEntidad.getEdadMinima() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("edadMinima"), tipoIdentificacionEntidad.getEdadMinima()));

				}
				
				if (tipoIdentificacionEntidad.getEdadMaxima() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("edadMaxima"), tipoIdentificacionEntidad.getEdadMaxima()));

				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultado el tipo de indentificacion");
		}

	}

}
