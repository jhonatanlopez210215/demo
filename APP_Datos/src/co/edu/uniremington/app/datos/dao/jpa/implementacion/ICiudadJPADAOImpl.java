package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import co.edu.uniremington.app.datos.dao.jpa.ICiudadJPADAOCustom;
import co.edu.uniremington.app.entidad.CiudadEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class ICiudadJPADAOImpl implements ICiudadJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<CiudadEntidad> consultar(CiudadEntidad ciudadEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<CiudadEntidad> consulta = constructorCondiciones.createQuery(CiudadEntidad.class);
			Root<CiudadEntidad> condicional = consulta.from(CiudadEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (ciudadEntidad != null) {

				if (ciudadEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), ciudadEntidad.getCodigo()));

				}

				if (ciudadEntidad.getNombre() != null && ciudadEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), ciudadEntidad.getNombre()));
				}
				
				if (ciudadEntidad.getDepartamento().getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("departamento"), ciudadEntidad.getDepartamento().getCodigo()));

				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultando ciudad");
		}

	}

}
