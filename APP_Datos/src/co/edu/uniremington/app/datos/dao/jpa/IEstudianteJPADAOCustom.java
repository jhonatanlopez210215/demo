package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.EstudianteEntidad;

@Repository
public interface IEstudianteJPADAOCustom {
	List<EstudianteEntidad> consultar(EstudianteEntidad estudianteEntidad);

}
