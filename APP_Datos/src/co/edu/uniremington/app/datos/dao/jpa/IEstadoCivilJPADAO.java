package co.edu.uniremington.app.datos.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
@Repository
public interface IEstadoCivilJPADAO extends JpaRepository<EstadoCivilEntidad, Integer>, IEstadoCivilJPADAOCustom {

}
