package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import co.edu.uniremington.app.datos.dao.jpa.IEstadoCivilJPADAOCustom;
import co.edu.uniremington.app.entidad.EstadoCivilEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class IEstadoCivilJPADAOImpl implements IEstadoCivilJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<EstadoCivilEntidad> consultar(EstadoCivilEntidad estadoCivilEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<EstadoCivilEntidad> consulta = constructorCondiciones.createQuery(EstadoCivilEntidad.class);
			Root<EstadoCivilEntidad> condicional = consulta.from(EstadoCivilEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (estadoCivilEntidad != null) {

				if (estadoCivilEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), estadoCivilEntidad.getCodigo()));

				}

				if (estadoCivilEntidad.getNombre() != null && estadoCivilEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), estadoCivilEntidad.getNombre()));
				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultando estado civil");
		}

	}

}
