package co.edu.uniremington.app.datos.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.DepartamentoEntidad;
@Repository
public interface IDepartamentoJPADAO extends JpaRepository<DepartamentoEntidad, Integer>, IDepartamentoJPADAOCustom {

}
