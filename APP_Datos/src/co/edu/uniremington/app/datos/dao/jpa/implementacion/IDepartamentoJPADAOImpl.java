package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import co.edu.uniremington.app.datos.dao.jpa.IDepartamentoJPADAOCustom;
import co.edu.uniremington.app.entidad.DepartamentoEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class IDepartamentoJPADAOImpl implements IDepartamentoJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<DepartamentoEntidad> consultar(DepartamentoEntidad departamentoEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<DepartamentoEntidad> consulta = constructorCondiciones.createQuery(DepartamentoEntidad.class);
			Root<DepartamentoEntidad> condicional = consulta.from(DepartamentoEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (departamentoEntidad != null) {

				if (departamentoEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), departamentoEntidad.getCodigo()));
				}

				if (departamentoEntidad.getNombre() != null && departamentoEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), departamentoEntidad.getNombre()));
				}

				if (departamentoEntidad.getPais().getCodigo() > 0) {
					predicados.add(constructorCondiciones.equal(condicional.get("pais"), departamentoEntidad.getPais().getCodigo()));
				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultando departamento");
		}

	}

}
