package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import co.edu.uniremington.app.datos.dao.jpa.IPaisJPADAOCustom;
import co.edu.uniremington.app.entidad.PaisEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class IPaisJPADAOImpl implements IPaisJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<PaisEntidad> consultar(PaisEntidad paisEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<PaisEntidad> consulta = constructorCondiciones.createQuery(PaisEntidad.class);
			Root<PaisEntidad> condicional = consulta.from(PaisEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (paisEntidad != null) {

				if (paisEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), paisEntidad.getCodigo()));

				}

				if (paisEntidad.getNombre() != null && paisEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), paisEntidad.getNombre()));
				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultando pais");
		}

	}

}
