package co.edu.uniremington.app.datos.dao.jpa.implementacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import co.edu.uniremington.app.datos.dao.jpa.IEstudianteJPADAOCustom;
import co.edu.uniremington.app.entidad.EstudianteEntidad;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDatos;

public class IEstudianteJPADAOImpl implements IEstudianteJPADAOCustom {

	@PersistenceContext
	private EntityManager gestorEntidades;

	@Override
	public List<EstudianteEntidad> consultar(EstudianteEntidad estudianteEntidad) {
		try {
			CriteriaBuilder constructorCondiciones = gestorEntidades.getCriteriaBuilder();
			CriteriaQuery<EstudianteEntidad> consulta = constructorCondiciones.createQuery(EstudianteEntidad.class);
			Root<EstudianteEntidad> condicional = consulta.from(EstudianteEntidad.class);
			List<Predicate> predicados = new ArrayList<>();

			if (estudianteEntidad != null) {

				if (estudianteEntidad.getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigo"), estudianteEntidad.getCodigo()));

				}
				
				if (estudianteEntidad.getTipoIdentificacion().getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("tipoIdentificacion"), estudianteEntidad.getTipoIdentificacion().getCodigo()));
				}
				

				if (estudianteEntidad.getNumeroIdentificacion() != null && estudianteEntidad.getNumeroIdentificacion().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("numeroIdentificacion"), estudianteEntidad.getNumeroIdentificacion()));
				}


				if (estudianteEntidad.getNombre() != null && estudianteEntidad.getNombre().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("nombre"), estudianteEntidad.getNombre()));
				}
				
				if (estudianteEntidad.getCorreoElectronico()!= null && estudianteEntidad.getCorreoElectronico().trim().equals("")) {
					// LIKE SOLO PARA DATOS STRING
					predicados.add(constructorCondiciones.like(condicional.get("correoElectronico"), estudianteEntidad.getCorreoElectronico()));
				}
				
				if (estudianteEntidad.getFechaNacimiento() != null && estudianteEntidad.getFechaNacimiento().toString().trim().equals("")) {
					predicados.add(constructorCondiciones.equal(condicional.get("fechaNacimiento"), estudianteEntidad.getFechaNacimiento()));
				}
				
				if (estudianteEntidad.getEstadoCivil().getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("estadoCivil"), estudianteEntidad.getEstadoCivil().getCodigo()));
				}
				
				if (estudianteEntidad.getCodigoCiudad().getCodigo() > 0) {
					// EQUAL PARA DATOS N�MERICOS
					predicados.add(constructorCondiciones.equal(condicional.get("codigoCiudad"), estudianteEntidad.getCodigoCiudad().getCodigo()));
				}

			}

			consulta.select(condicional).where(constructorCondiciones.and(predicados.toArray(new Predicate[0])));
			return gestorEntidades.createQuery(consulta).getResultList();

		} catch (Exception e) {
			throw new ExcepcionesDatos("Ha ocurrido un error consultando estudiante");
		}

	}

}
