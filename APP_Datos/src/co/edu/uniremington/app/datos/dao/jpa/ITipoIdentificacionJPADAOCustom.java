package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;

@Repository
public interface ITipoIdentificacionJPADAOCustom {
	List<TipoIdentificacionEntidad> consultar(TipoIdentificacionEntidad tipoIdentificacionEntidad);
	

}
