package co.edu.uniremington.app.datos.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.TipoIdentificacionEntidad;

@Repository
public interface ITipoIdentificacionJPADAO extends JpaRepository<TipoIdentificacionEntidad, Integer>, ITipoIdentificacionJPADAOCustom {

}
