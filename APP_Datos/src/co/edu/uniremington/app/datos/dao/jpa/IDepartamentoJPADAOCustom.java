package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.DepartamentoEntidad;
@Repository
public interface IDepartamentoJPADAOCustom {
	List<DepartamentoEntidad> consultar(DepartamentoEntidad departamentoEntidad);
	

}
