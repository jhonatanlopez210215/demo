package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.CiudadEntidad;

@Repository
public interface ICiudadJPADAOCustom {
	List<CiudadEntidad> consultar(CiudadEntidad ciudadEntidad);
	

}
