package co.edu.uniremington.app.datos.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.PaisEntidad;

@Repository
public interface IPaisJPADAO extends JpaRepository<PaisEntidad, Integer>, IPaisJPADAOCustom {

}
