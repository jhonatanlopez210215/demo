package co.edu.uniremington.app.datos.dao.jpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import co.edu.uniremington.app.entidad.EstadoCivilEntidad;

@Repository
public interface IEstadoCivilJPADAOCustom {
	List<EstadoCivilEntidad> consultar(EstadoCivilEntidad estadoCivilEntidad);
	

}
