package co.edu.uniremington.app.dominio;


import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDominio;

public class DepartamentoDominio {

	private int codigo;
	private String nombre;
	private PaisDominio pais;
	private OperacionEnumerador operacion;

	private DepartamentoDominio() {
		super();
	}

	public static DepartamentoDominio instanciaParaCreacion(String nombre, PaisDominio paisDominio) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.CREACION);
		dominio.setPais(paisDominio);
		dominio.setNombre(nombre);

		return dominio;
	}

	public static DepartamentoDominio instanciaParaFiltro(int codigo, String nombre, PaisDominio paisDominio) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.FILTRO);
		if (codigo > 0) {
			dominio.setCodigo(codigo);
		}

		if (nombre != null) {
			dominio.setNombre(nombre);
		}
		
		if (paisDominio != null) {
			dominio.setPais(paisDominio);
		}

		return dominio;
	}

	public static DepartamentoDominio instanciaParaActualizacion(int codigo, String nombre, PaisDominio paisDominio) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.ACTUALIZACION);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setPais(paisDominio);

		return dominio;
	}

	public static DepartamentoDominio instanciaParaEliminacion(int codigo) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.ELIMINACION);
		dominio.setCodigo(codigo);

		return dominio;
	}

	public static DepartamentoDominio instanciaParaPoblamiento(int codigo, String nombre, PaisDominio paisDominio) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.POBLAMIENTO);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setPais(paisDominio);

		return dominio;
	}

	public static DepartamentoDominio instanciaParaDependencia(int codigo) {
		DepartamentoDominio dominio = new DepartamentoDominio();
		dominio.setOperacion(OperacionEnumerador.DEPENDENCIA);
		dominio.setCodigo(codigo);

		return dominio;
	}

	private void setCodigo(int codigo) {
		if (codigo <= 0) {
			throw new ExcepcionesDominio(
					"El codigo de un departamento no puede contener valores menores o iguales a cero");
		}

		this.codigo = codigo;

	}

	private void setNombre(String nombre) {

		// Nombre no est� vac�o
		if (nombre == null || nombre.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre de un departamento no puede estar vac�o");

		}

		// Longitud del nomnbre
		if (nombre.trim().length() > 100) {
			throw new ExcepcionesDominio("El nombre de un departamento no puede tener m�s de 100 caracteres");

		}

		// Formato del nomnbre

		if (!nombre.trim().matches("^[�-zA-Z������������ ]+$")) {
			throw new ExcepcionesDominio("El nombre del departamento solo puede contener letras");

		}

		this.nombre = nombre.trim();
	}

	private void setPais(PaisDominio pais) {
		
		if (pais.getCodigo() <= 0) {
			throw new ExcepcionesDominio("El pais debe tener un codigo mayor que cero");
		}
		
		this.pais = pais;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		if (nombre == null) {
			nombre = "";
		}
		return nombre;
	}

	public PaisDominio getPais() {
		return pais;
	}

	private void setOperacion(OperacionEnumerador operacion) {
		if (operacion == null) {
			throw new ExcepcionesDominio("La operaci�n es obligatoria para crear el pais ...");
		}

		this.operacion = operacion;
	}

	public OperacionEnumerador getOperacion() {
		return operacion;
	}

}
