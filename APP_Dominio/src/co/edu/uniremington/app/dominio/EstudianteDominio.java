package co.edu.uniremington.app.dominio;

import java.util.Date;

import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDominio;

public class EstudianteDominio {

	private int codigo;
	private TipoIdentificacionDominio tipoIdentificacion;
	private String numeroIdentificacion;
	private String nombre;
	private String correoElectronico;
	private Date fechaNacimiento;
	private EstadoCivilDominio estadoCivil;
	private String direccion;
	private CiudadDominio codigoCiudad;
	private OperacionEnumerador operacion;

	private EstudianteDominio() {
		super();
	}
	// CREACION - FILTRO - ACTUALIZACION - ELIMINACION - POBLACION - DEPENDENCIA

	public static EstudianteDominio instanciaParaCreacion(String nombre, TipoIdentificacionDominio tipoIdentificacion, String numeroIdentificacion, String correoElectronico,
			Date fechaNacimiento, EstadoCivilDominio estadoCivil, String direccion, CiudadDominio codigoCiudad) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.CREACION);
		dominio.setNombre(nombre);
		dominio.setTipoIdentificacion(tipoIdentificacion);
		dominio.setNumeroIdentificacion(numeroIdentificacion);
		dominio.setCorreoElectronico(correoElectronico);
		dominio.setFechaNacimiento(fechaNacimiento);
		dominio.setEstadoCivil(estadoCivil);
		dominio.setDireccion(direccion);
		dominio.setCodigoCiudad(codigoCiudad);

		return dominio;
	}

	public static EstudianteDominio instanciaParaFiltro(int codigo, String nombre, TipoIdentificacionDominio tipoIdentificacion, String numeroIdentificacion,
			String correoElectronico,Date fechaNacimiento, EstadoCivilDominio estadoCivil, String direccion, CiudadDominio codigoCiudad) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.FILTRO);
		if (codigo > 0) {
			dominio.setCodigo(codigo);
		}

		if (nombre != null) {
			dominio.setNombre(nombre);
		}
		
		if (tipoIdentificacion.getCodigo() > 0) {
			dominio.setTipoIdentificacion(tipoIdentificacion);
		}
		
		if (numeroIdentificacion != null) {
			dominio.setNumeroIdentificacion(numeroIdentificacion);
		}
		
		if (correoElectronico != null) {
			dominio.setCorreoElectronico(correoElectronico);
		}
		
		if (fechaNacimiento != null) {
			dominio.setFechaNacimiento(fechaNacimiento);
		}
		
		if(estadoCivil.getCodigo() > 0) {
			dominio.setEstadoCivil(estadoCivil);
		}
		
		if (direccion != null) {
			dominio.setDireccion(direccion);
		}
		
		if(codigoCiudad.getCodigo() > 0) {
			dominio.setCodigoCiudad(codigoCiudad);
		}
		

		return dominio;
	}

	public static EstudianteDominio instanciaParaActualizacion(int codigo, String nombre, TipoIdentificacionDominio tipoIdentificacion, String numeroIdentificacion,
			String correoElectronico,Date fechaNacimiento, EstadoCivilDominio estadoCivil, String direccion, CiudadDominio codigoCiudad) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.ACTUALIZACION);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setTipoIdentificacion(tipoIdentificacion);
		dominio.setNumeroIdentificacion(numeroIdentificacion);
		dominio.setCorreoElectronico(correoElectronico);
		dominio.setFechaNacimiento(fechaNacimiento);
		dominio.setEstadoCivil(estadoCivil);
		dominio.setDireccion(direccion);
		dominio.setCodigoCiudad(codigoCiudad);

		return dominio;
	}

	public static EstudianteDominio instanciaParaEliminacion(int codigo) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.ELIMINACION);
		dominio.setCodigo(codigo);

		return dominio;
	}

	public static EstudianteDominio instanciaParaPoblamiento(int codigo, String nombre, TipoIdentificacionDominio tipoIdentificacion, String numeroIdentificacion,
			String correoElectronico,Date fechaNacimiento, EstadoCivilDominio estadoCivil, String direccion, CiudadDominio codigoCiudad) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.POBLAMIENTO);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setTipoIdentificacion(tipoIdentificacion);
		dominio.setNumeroIdentificacion(numeroIdentificacion);
		dominio.setCorreoElectronico(correoElectronico);
		dominio.setFechaNacimiento(fechaNacimiento);
		dominio.setEstadoCivil(estadoCivil);
		dominio.setDireccion(direccion);
		dominio.setCodigoCiudad(codigoCiudad);

		return dominio;
	}

	public static EstudianteDominio instanciaParaDependencia(int codigo) {
		EstudianteDominio dominio = new EstudianteDominio();
		dominio.setOperacion(OperacionEnumerador.DEPENDENCIA);
		dominio.setCodigo(codigo);

		return dominio;
	}

	// SETTERS

	private void setCodigo(int codigo) {
		if (codigo <= 0) {
			throw new ExcepcionesDominio("El codigo del estudiante no puede contener valores menores o iguales a cero");
		}
		this.codigo = codigo;
	}

	private void setTipoIdentificacion(TipoIdentificacionDominio tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	private void setNumeroIdentificacion(String numeroIdentificacion) {

		// numeroIndentificacion no est� vac�o
		if (numeroIdentificacion == null || numeroIdentificacion.trim().equals("")) {
			throw new ExcepcionesDominio("El n�mero de identificacion no puede estar vac�a");

		}

		// Longitud del numeroIdentificacion
		if (numeroIdentificacion.trim().length() > 20) {
			throw new ExcepcionesDominio(
					"El longitud del numero de indenfiticacion no puede tener m�s de 100 caracteres");

		}

		// Formato del numeroIdentificacion

		if (!numeroIdentificacion.trim().matches("^[1-0 ]+$")) {
			throw new ExcepcionesDominio("El numero de identificacion solo puede contener n�meros");

		}
		this.numeroIdentificacion = numeroIdentificacion;
	}

	private void setNombre(String nombre) {

		// Nombre no est� vac�o
		if (nombre == null || nombre.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre del estudiante no puede estar vac�o");

		}

		// Longitud del nomnbre
		if (nombre.trim().length() > 200) {
			throw new ExcepcionesDominio("El nombre del estudiante no puede tener m�s de 200 caracteres");

		}

		// Formato del nomnbre

		if (!nombre.trim().matches("^[�-zA-Z������������ ]+$")) {
			throw new ExcepcionesDominio("El nombre del estudiante solo puede contener letras");

		}

		this.nombre = nombre.trim();
	}

	private void setCorreoElectronico(String correoElectronico) {
		// correoElectronico no est� vac�o
		if (correoElectronico == null || correoElectronico.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre del correo no puede estar vac�a");

		}

		// Longitud del correoElectronico
		if (correoElectronico.trim().length() > 255) {
			throw new ExcepcionesDominio("El nombre del correo no puede tener m�s de 255 caracteres");

		}

		this.correoElectronico = correoElectronico;
	}

	private void setFechaNacimiento(Date fechaNacimiento) {
		if (fechaNacimiento == null) {
			throw new ExcepcionesDominio("La fecha de nacimiento no puede estar vac�a");
		}

		this.fechaNacimiento = fechaNacimiento;
	}

	private void setEstadoCivil(EstadoCivilDominio estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	private void setDireccion(String direccion) {
		// direccion no est� vac�o
		if (direccion == null || direccion.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre de la direccion no puede estar vac�o");

		}

		// Longitud del direccion
		if (direccion.trim().length() > 50) {
			throw new ExcepcionesDominio("La logitud de la direcci�n no puede tener m�s de 50 caracteres");

		}
		this.direccion = direccion;
	}

	private void setCodigoCiudad(CiudadDominio codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}

	// GETTERS
	public int getCodigo() {
		return codigo;
	}

	public TipoIdentificacionDominio getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		if (numeroIdentificacion == null) {
			numeroIdentificacion = "";
		}
		return numeroIdentificacion;
	}

	public String getNombre() {
		if (nombre == null) {
			nombre = "";
		}
		return nombre;
	}

	public String getCorreoElectronico() {
		if (correoElectronico == null) {
			correoElectronico = "";
		}
		return correoElectronico;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public EstadoCivilDominio getEstadoCivil() {
		return estadoCivil;
	}

	public String getDireccion() {
		if (direccion == null) {
			direccion = "";
		}
		return direccion;
	}

	public CiudadDominio getCodigoCiudad() {
		return codigoCiudad;
	}

	public void setOperacion(OperacionEnumerador operacion) {
		if (operacion == null) {
			throw new ExcepcionesDominio("La operaci�n es obligatoria para crear el estudiante ...");
		}

		this.operacion = operacion;
	}

	public OperacionEnumerador getOperacion() {
		return operacion;
	}

}
