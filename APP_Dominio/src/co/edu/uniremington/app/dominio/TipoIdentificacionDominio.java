package co.edu.uniremington.app.dominio;


import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDominio;

public class TipoIdentificacionDominio {

	private int codigo;
	private String nombre;
	private int edadMinima;
	private int edadMaxima;
	private OperacionEnumerador operacion;

	private TipoIdentificacionDominio() {
		super();
	}

	public static TipoIdentificacionDominio instanciaParaCreacion(String nombre, int edadMinima, int edadMaxima) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.CREACION);
		dominio.setNombre(nombre);
		dominio.setEdadMinima(edadMinima);
		dominio.setEdadMaxima(edadMaxima);

		return dominio;
	}

	public static TipoIdentificacionDominio instanciaParaFiltro(int codigo, String nombre, int edadMinima, int edadMaxima) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.FILTRO);
		if (codigo > 0) {
			dominio.setCodigo(codigo);
		}

		if (nombre != null) {
			dominio.setNombre(nombre);
		}
		
		if (edadMinima > 0) {
			dominio.setEdadMinima(edadMinima);
		}
		
		if (edadMaxima < 150) {
			dominio.setEdadMaxima(edadMaxima);
		}

		return dominio;
	}

	public static TipoIdentificacionDominio instanciaParaActualizacion(int codigo, String nombre, int edadMinima, int edadMaxima) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.ACTUALIZACION);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setEdadMinima(edadMinima);
		dominio.setEdadMaxima(edadMaxima);


		return dominio;
	}

	public static TipoIdentificacionDominio instanciaParaEliminacion(int codigo) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.ELIMINACION);
		dominio.setCodigo(codigo);

		return dominio;
	}

	public static TipoIdentificacionDominio instanciaParaPoblamiento(int codigo, String nombre, int edadMinima, int edadMaxima) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.POBLAMIENTO);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setEdadMinima(edadMinima);
		dominio.setEdadMaxima(edadMaxima);

		return dominio;
	}

	public static TipoIdentificacionDominio instanciaParaDependencia(int codigo) {
		TipoIdentificacionDominio dominio = new TipoIdentificacionDominio();
		dominio.setOperacion(OperacionEnumerador.DEPENDENCIA);
		dominio.setCodigo(codigo);

		return dominio;
	}

	private void setCodigo(int codigo) {
		if (codigo <= 0) {
			throw new ExcepcionesDominio(
					"El codigo del tipo de identificacion no puede contener valores menores o iguales a cero");
		}

		this.codigo = codigo;

	}

	private void setNombre(String nombre) {

		// Nombre no est� vac�o
		if (nombre == null || nombre.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre del tipo de identificacion no puede estar vac�a");

		}

		// Longitud del nombre
		if (nombre.trim().length() > 100) {
			throw new ExcepcionesDominio("El nombre del tipo de identificacion no puede tener m�s de 100 caracteres");

		}

		// Formato del nombre

		if (!nombre.trim().matches("^[�-zA-Z������������ ]+$")) {
			throw new ExcepcionesDominio("El nombre del tipo de identificacion solo puede contener letras");

		}

		this.nombre = nombre.trim();
	}

	private void setEdadMinima(int edadMinima) {
		if (edadMinima <= 0) {
			throw new ExcepcionesDominio("La edad minima permitada es de 1 a�o en adelante");
		}

		this.edadMinima = edadMinima;
	}

	private void setEdadMaxima(int edadMaxima) {
		if (edadMaxima > 150) {
			throw new ExcepcionesDominio("La edad maxima permitida es de 150 a�os");
		}

		this.edadMaxima = edadMaxima;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		if (nombre == null) {
			nombre = "";
		}
		return nombre;

	}

	public int getEdadMinima() {
		return edadMinima;
	}

	public int getEdadMaxima() {
		return edadMaxima;
	}

	private void setOperacion(OperacionEnumerador operacion) {
		if (operacion == null) {
			throw new ExcepcionesDominio("La operaci�n es obligatoria para crear el tipo de identificacion ...");
		}

		this.operacion = operacion;
	}

	public OperacionEnumerador getOperacion() {
		return operacion;
	}

}
