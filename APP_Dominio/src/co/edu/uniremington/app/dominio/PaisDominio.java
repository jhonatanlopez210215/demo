package co.edu.uniremington.app.dominio;


import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDominio;

public class PaisDominio {

	private int codigo;
	private String nombre;
	private OperacionEnumerador operacion;

	private PaisDominio() {
		super();
	}

	public static PaisDominio instanciaParaCreacion(String nombre) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.CREACION);
		dominio.setNombre(nombre);

		return dominio;
	}

	public static PaisDominio instanciaParaFiltro(int codigo, String nombre) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.FILTRO);
		if (codigo > 0) {
			dominio.setCodigo(codigo);
		}

		if (nombre != null) {
			dominio.setNombre(nombre);
		}

		return dominio;
	}

	public static PaisDominio instanciaParaActualizacion(int codigo, String nombre) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.ACTUALIZACION);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);

		return dominio;
	}

	public static PaisDominio instanciaParaEliminacion(int codigo) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.ELIMINACION);
		dominio.setCodigo(codigo);

		return dominio;
	}

	public static PaisDominio instanciaParaPoblamiento(int codigo, String nombre) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.POBLAMIENTO);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);

		return dominio;
	}

	public static PaisDominio instanciaParaDependencia(int codigo) {
		PaisDominio dominio = new PaisDominio();
		dominio.setOperacion(OperacionEnumerador.DEPENDENCIA);
		dominio.setCodigo(codigo);

		return dominio;
	}

	private void setCodigo(int codigo) {
		if (codigo <= 0) {
			throw new ExcepcionesDominio("El codigo de un pais no puede contener valores menores o iguales a cero");
		}

		this.codigo = codigo;

	}

	private void setNombre(String nombre) {

		// Nombre no est� vac�o
		if (nombre == null || nombre.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre del pais no puede estar vac�o");

		}

		// Longitud del nomnbre
		if (nombre.trim().length() > 100) {
			throw new ExcepcionesDominio("El nombre del pais no puede tener m�s de 100 caracteres");

		}

		// Formato del nomnbre

		if (!nombre.trim().matches("^[�-zA-Z������������ ]+$")) {
			throw new ExcepcionesDominio("El nombre del pais solo puede contener letras");

		}

		this.nombre = nombre.trim();
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		if (nombre == null) {
			nombre = "";
		}
		return nombre;

	}

	private void setOperacion(OperacionEnumerador operacion) {
		if (operacion == null) {
			throw new ExcepcionesDominio("La operaci�n es obligatoria para crear el pais ...");
		}

		this.operacion = operacion;
	}

	public OperacionEnumerador getOperacion() {
		return operacion;
	}

}