package co.edu.uniremington.app.dominio;

import co.edu.uniremington.app.transversal.enumerador.operacion.OperacionEnumerador;
import co.edu.uniremington.app.transversal.excepciones.ExcepcionesDominio;

public class CiudadDominio {

	private int codigo;
	private String nombre;
	private DepartamentoDominio departamento;
	private OperacionEnumerador operacion;

	private CiudadDominio() {
		super();
	}

	public static CiudadDominio instanciaParaCreacion(String nombre, DepartamentoDominio departamentoDominio) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.CREACION);
		dominio.setNombre(nombre);
		dominio.setDepartamento(departamentoDominio);

		return dominio;
	}

	public static CiudadDominio instanciaParaFiltro(int codigo, String nombre, DepartamentoDominio departamentoDominio) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.FILTRO);
		if (codigo > 0) {
			dominio.setCodigo(codigo);
		}

		if (nombre != null) {
			dominio.setNombre(nombre);
		}
		
		if(departamentoDominio.getCodigo() > 0) {
			dominio.setDepartamento(departamentoDominio);
		}

		return dominio;
	}

	public static CiudadDominio instanciaParaActualizacion(int codigo, String nombre, DepartamentoDominio departamentoDominio) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.ACTUALIZACION);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setDepartamento(departamentoDominio);

		return dominio;
	}

	public static CiudadDominio instanciaParaEliminacion(int codigo) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.ELIMINACION);
		dominio.setCodigo(codigo);

		return dominio;
	}

	public static CiudadDominio instanciaParaPoblamiento(int codigo, String nombre, DepartamentoDominio departamentoDominio) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.POBLAMIENTO);
		dominio.setCodigo(codigo);
		dominio.setNombre(nombre);
		dominio.setDepartamento(departamentoDominio);

		return dominio;
	}

	public static CiudadDominio instanciaParaDependencia(int codigo) {
		CiudadDominio dominio = new CiudadDominio();
		dominio.setOperacion(OperacionEnumerador.DEPENDENCIA);
		dominio.setCodigo(codigo);

		return dominio;
	}

	private void setCodigo(int codigo) {
		try {
			if (codigo <= 0) {
				throw new ExcepcionesDominio("El codigo de la ciudad no puede contener valores menores o iguales a cero");
			}
		} catch (Exception e) {
			throw new ExcepcionesDominio(e.getMessage());
		}
		this.codigo = codigo;

	}

	private void setNombre(String nombre) {
		// Nombre no est� vac�o
		if (nombre == null || nombre.trim().equals("")) {
			throw new ExcepcionesDominio("El nombre de la ciudad no puede estar vac�a");

		}
		

		// Longitud del nomnbre
		if (nombre.trim().length() > 100) {
			throw new ExcepcionesDominio("El nombre de la ciudad no puede tener m�s de 100 caracteres");

		}

		// Formato del nomnbre

		if (!nombre.trim().matches("^[�-zA-Z������������ ]+$")) {
			throw new ExcepcionesDominio("El nombre de la ciudad solo puede contener letras");

		}
		

		this.nombre = nombre.trim();
	}

	private void setDepartamento(DepartamentoDominio departamento) {
		try {
			if (departamento.getCodigo() <= 0) {
				throw new ExcepcionesDominio("El codigo del departamento no puede ser menor que cero");
			}
		} catch (Exception e) {
			throw new ExcepcionesDominio(e.getMessage());
		}
		this.departamento = departamento;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNombre() {
		if (nombre == null) {
			nombre = "";
		}
		return nombre;
	}

	public DepartamentoDominio getDepartamento() {
		return departamento;
	}

	private void setOperacion(OperacionEnumerador operacion) {
		if (operacion == null) {
			throw new ExcepcionesDominio("La operaci�n es obligatoria para crear la ciudad ...");
		}

		this.operacion = operacion;
	}

	public OperacionEnumerador getOperacion() {
		return operacion;
	}

}
