package co.edu.uniremington.app.transversal.excepciones;

public class ExcepcionesTransversal extends RuntimeException{
	
	public ExcepcionesTransversal(String mensaje) {
		super(mensaje);

	}

}
